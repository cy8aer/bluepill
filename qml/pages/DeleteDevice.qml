import QtQuick 2.2
import Sailfish.Silica 1.0

Dialog {
    id: thedialog

    property string user_id
    property string device_id
    property string display_name
    property string password
    property int timeoutval: 30

    Component.onCompleted: {
        clienthandler.getUserProfile(user_id)
    }

    onDone: {
         if (result == DialogResult.Accepted) {
             password = passwordField.text
         }
    }

    Column {
        width: parent.width
        anchors.margins: Theme.paddingMedium

        DialogHeader { title: qsTr('Delete device')}

        Label {
            text: qsTr('Device ID')
            color: Theme.highlightColor
            padding: Theme.paddingMedium
        }

        Label {
            width: parent.width
            height: contentHeight
            wrapMode: Text.WordWrap
            padding: Theme.paddingMedium
            id: thedeviceid
            text: device_id
        }

        Label {
            text: qsTr('Device Name')
            color: Theme.highlightColor
            padding: Theme.paddingMedium
        }

        Label {
            width: parent.width
            height: contentHeight
            wrapMode: Text.WordWrap
            padding: Theme.paddingMedium
            id: thename
            text: display_name
        }

        Label {
            text: qsTr('Password')
            color: Theme.highlightColor
            padding: Theme.paddingMedium
        }

        PasswordField {
            id: passwordField
        }
    }
}
