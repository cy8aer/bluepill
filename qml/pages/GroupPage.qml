import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    property bool isloading: true
    property bool isroom: true

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All


    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaListView {
        anchors.fill: parent

        model: ListModel {
            id: roomsModel
        }
        delegate: RoomListItem { }

        PullDownMenu {
            id: pulldown

            MenuItem {
                text: qsTr('Enter room')
                onClicked: pageStack.push(Qt.resolvedUrl("JoinRoom.qml"))
            }
            MenuItem {
                text: qsTr('Create room')
            }
            MenuItem {
                text: qsTr('StartChat')
            }
            MenuItem {
                text: qsTr("Preferences")
                onClicked: pageStack.push(Qt.resolvedUrl("Preferences.qml"))
            }
        }

        Component.onCompleted: {
            clienthandler.getRoomList()
        }

        Connections {
            target: clienthandler
            onRoomList: {
                isloading = false
                roomsModel.clear()
                console.log(data.length, "length")
                for (var i = 0; i < data.length; i++) {
                    if (!data[i].is_group) {
                        roomsModel.append(data[i])
                    }
                }
            }
            onRoomLeaveResponse: {
                isloading = false
                roomsModel.clear()
                console.log(data.length, "length")
                for (var i = 0; i < data.length; i++) {
                    if (!data[i].is_group) {
                        roomsModel.append(data[i])
                    }
                }
            }
            onNewMessage: {
                clienthandler.getRoomList()
            }
        }

        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height + column.spacing + roomslist.height + roomslist.spacing

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.

        BusyIndicator {
            size: BusyIndicatorSize.Large
            anchors.centerIn: parent
            running: isloading
        }

        header: PageHeader {
                    id: pagetitle
                    title: qsTr("People")
                }

        ListView {
                id: roomslist
                width: parent.width
                height: parent.height - pagetitle.height
                anchors.margins: Theme.paddingMedium
                // orientation: ListView.HorizontalAndVerticalFlick
        }
    }
}
