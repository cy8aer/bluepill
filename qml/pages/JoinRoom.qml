import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    id: page
    allowedOrientations: Orientation.All

    SilicaFlickable {
        id: loginflick
        anchors.fill: parent
        contentHeight: joinRoomTitle.height + joinRoomTitle.spacing + parameters.height + parameters.spacing
        DialogHeader {
            id: dialogheader
        }

        Connections {
            target: clienthandler
            onLoggedOut: {
                pageStack.replace(Qt.resolvedUrl("LoginPage.qml"))
            }
        }

        Column {
            id: joinRoomTitle

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Join Room")
            }
        }


        Column {
            id: parameters
            width: parent.width
            height: children.height
            anchors {
                top: joinRoomTitle.bottom
                left: parent.left
                right: parent.right
                margins: Theme.paddingMedium
            }
            TextField {
                id: room_id_or_field
                focus: true
                text: ""
                placeholderText: "#room:host.com"
            }
        }
    }
    onAccepted: {
        if (room_id_or_field.text != "") {
            clienthandler.joinRoom(room_id_or_field.text)
        }
    }
}
