import QtQuick 2.2
import Sailfish.Silica 1.0

Dialog {
    id: thedialog

    property var emojis
    property int timeoutval: 30

    Component.onCompleted: {
        for (var i = 0; i < emojis.length; i++) {
                emojimodel.append({ emoji: emojis[i][0], emotext: emojis[i][1] })
            }
    }

    onTimeoutvalChanged: {
        if (timeoutval == 0) {
            thedialog.reject()
        }
    }

    Column {
        id: textcol
        width: parent.width
        anchors.margins: Theme.paddingMedium

        DialogHeader { title: qsTr('Compare Icons')}

        Item {
              Timer {
                  interval: 1000; running: true; repeat: true
                  onTriggered: timeoutval -= 1
              }
          }

        ProgressBar {
            id: thetimeout
            width: parent.width
            height: Theme.itemSizeSmall
            minimumValue: 0
            maximumValue: 30
            value: timeoutval
        }


   }
    SilicaGridView {
        id: emojigrid
        anchors.top: textcol.bottom
        anchors.margins: Theme.paddingMedium
        height: contentHeight
        cellHeight: Theme.itemSizeHuge
        cellWidth: Theme.itemSizeLarge
        flow: GridView.FlowLeftToRight
        layoutDirection: "LeftToRight"
        verticalLayoutDirection: "TopToBottom"

        width: parent.width

        model: ListModel {
            id: emojimodel
        }

        delegate: Item {
            width: GridView.view.width
            height: Theme.itemSizeHuge

            Column {
                anchors.margins: Theme.paddingMedium
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                Label {
                    width: parent.width
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: emoji
                    font.pixelSize: Theme.fontSizeHuge
                }
                Label {
                    width: parent.width
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: emotext
                    font.pixelSize: Theme.fontSizeTiny
                    wrapMode: Text.WordWrap
                }
            }
        }
    }
//    onDone: {
//        if (result == DialogResult.Accepted) {
//            name = nameField.text
//        }
//    }
}
