import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    property string rid
    property string evid
    property string bd

    Column {
        id: redactreason
        width: parent.width
        spacing: Theme.paddingMedium

        TextField {
            id: redactField
            width: parent.width
            placeholderText: qsTr("Reason for redaction")
            label: qsTr("Reason")
        }

        Label {
            id: warningLabel
            anchors.margins: Theme.paddingMedium
            text: qsTr("Do you really wish to redact (delete) this event? This cannot be undone.")
            wrapMode: Text.WordWrap
            width: parent.width
            font.pixelSize: Theme.fontSizeMedium
            color: Theme.highlightColor
        }
    }

    Column {
        anchors.top: redactreason.bottom

        width: parent.width

        Label {
            text: bd
            anchors {
                left: parent.left
                right: parent.right
                margins: Theme.paddingMedium
            }
            wrapMode: Text.WordWrap
            // height: contentHeight
            width: parent.width
            font.pixelSize: Theme.fontSizeMedium
        }
    }

    onDone: {
        if (result === DialogResult.Accepted) {
            clienthandler.redactMessage(rid, evid, redactField.text)
        }
    }
}
