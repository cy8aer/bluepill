import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Connections {
        target: clienthandler
        onNeedLogin: {
            pageStack.replace(Qt.resolvedUrl("LoginPage.qml"))
        }
        onIsConnected: {
            statusLabel.text = qsTr("Connected...")
        }
        onConnectFailed: {
            statusLabel.text = qsTr("Not connected, trying again later...")
        }
        onInitReady: {
            console.log("Own user id: ", userid)
            bluepill.myuser_id = userid
            bluepill.mydevice_id = deviceid
            pageStack.replace(Qt.resolvedUrl("DashboardPage.qml"))
        }
    }

    allowedOrientations: Orientation.All

    SilicaFlickable {
        anchors.fill: parent

        contentHeight: parent.height

        Image {
            id: logo
            anchors.centerIn: parent
            source: "../../images/matrix_logo.svg"
            fillMode: Image.PreserveAspectFit
            sourceSize.width: parent.width * 0.66
        }
        Label {
            id: statusLabel
            anchors.bottom: parent.bottom
            padding: Theme.paddingMedium
            text: qsTr("Starting engine...")
            font.pixelSize: Theme.fontSizeTiny
            color: Theme.highlightColor
        }
    }
}
