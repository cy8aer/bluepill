import QtQuick 2.0
import Sailfish.Silica 1.0
import QtGraphicalEffects 1.0

ListItem {
    id: roomItem
    width: parent.width
    // height: Theme.itemSizeLarge
    contentHeight: Theme.itemSizeExtraLarge
    anchors.margins: Theme.paddingMedium
    onClicked: {
        pageStack.push(Qt.resolvedUrl("../pages/RoomPage.qml"),
                       {
                           room_id: room_id,
                           room_name: display_name
                       })
    }

    RemorseItem { id: remorse }

    menu: ContextMenu {
        id: contextmenu
        onActiveChanged: {
            panel.open = !active
        }
        MenuItem {
            id: favmenu
            text: qsTr('Favourite')
            // font.bold: prio && prio == "fav"
        }
        MenuItem {
            id: lowmenu
            text: qsTr('Low priority')
            // font.bold: prio && prio == "low"
        }
        MenuLabel {
            text: "---"
        }
        MenuItem {
            text: qsTr('Leave')
            onClicked: {
                remorse.execute(roomItem, qsTr("leaving room"), function() {
                    clienthandler.roomLeave(room_id)
                })
            }
        }
        MenuItem {
            text: qsTr('Preferences')
        }
    }

    Component.onCompleted: {
        // clienthandler.getUnreadCount(room_id)
    }

    Connections {
        target: clienthandler
        onTypingNotice: {
            console.log(room_id, roomId)
            if (room_id !== roomId) {
                return
            }
            blinkAnim.running = (userIds.length > 0)
        }
        onRoomUnread: {
            if (room_id === roomId) {
                countrec.visible = (count !== 0)
                unread.text = count !== 0 ? count : ""
            }
        }
    }


    Item {
        width: parent.width
        height: Theme.itemSizeLarge
        anchors.margins: Theme.paddingMedium

        clip: true

        Item {
            width: Theme.iconSizeMedium + 2*Theme.paddingMedium
            height: Theme.iconSizeMedium
            id: itemrec
            UserIcon {
                anchors.horizontalCenter: parent.horizontalCenter
                avatarurl: avatar_url ? avatar_url : ""
                // idirect: direct
                roomname: display_name
                uid: display_name
                width: Theme.iconSizeMedium
                height: Theme.iconSizeMedium
                id: therec

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("Room clicked", room_name)
                        pageStack.push(Qt.resolvedUrl("../pages/RoomPage.qml"), { room_id: room_id, room_name: room_name } )
                    }
                    onPressAndHold: openMenu()
                }
                Image {
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    width: Theme.iconSizeExtraSmall
                    height: Theme.iconSizeExtraSmall
                    source: "image://theme/icon-s-secure"
                    visible: encrypted
                }
                SequentialAnimation {
                    id: blinkAnim
                    running: false
                    loops: Animation.Infinite

                    NumberAnimation {
                        target: itemrec
                        property: "opacity"
                        duration: 1000
                        to: 0.5
                    }
                    NumberAnimation {
                        target: itemrec
                        property: "opacity"
                        duration: 1000
                        to: 1.0
                    }
                }
                Rectangle {
                    id: countrec
                    visible: false
                    z: 2
                    color: "grey"
                    anchors.top: therec.top
                    anchors.right: therec.right
                    width: Theme.itemSizeExtraSmall / 2
                    height: Theme.itemSizeExtraSmall / 2
                    radius: width * 0.5
                    Label {
                        anchors.centerIn: parent
                        id: unread
                        font.pixelSize: Theme.fontSizeTiny
                        font.bold: true
                        text: ""
                    }
                }
            }
        }


        Column {
            anchors.right: parent.right
            width: parent.width - itemrec.width - 2* Theme.paddingMedium
            anchors.margins: Theme.paddingMedium
//            Row {
//                Label {
//                    id: dstring
//                    text: new Date(origin_server_ts * 1000).toLocaleTimeString()
//                    font.pixelSize: Theme.fontSizeTiny
//                    font.bold: true
//                    color: Theme.highlightColor
//                    horizontalAlignment: Text.AlignRight
//                    verticalAlignment: Text.AlignTop
//                    padding: Theme.paddingSmall
//                }
//            }
            Label {
                id: dname
                text: display_name
                width: parent.width
                // height: Theme.fontSizeSmall
                font.pixelSize: Theme.fontSizeTiny
                font.bold: true
                truncationMode: TruncationMode.Fade
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignTop
                // padding: Theme.paddingSmall
            }

            Label {
                text: sender_name + ": " + event.content.body
                width: parent.width
                maximumLineCount: 2
                wrapMode: Text.WordWrap
                font.pixelSize: Theme.fontSizeTiny
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignTop
                // padding: Theme.paddingSmall
            }
            Label {
                id: dtime
                // height: Theme.fontSizeSmall
                width: parent.width
                text: {
                    return(bluepill.prettyDate(origin_server_ts))
                }

                font.pixelSize: Theme.fontSizeTiny
                color: Theme.highlightColor
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignTop
                padding: Theme.paddingSmall
            }
        }
    }
}
