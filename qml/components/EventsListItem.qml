import QtQuick 2.5
import Sailfish.Silica 1.0

ListItem {
    property string avatarUrl: ""
    property string  displayName: sender
    width: parent.width
    anchors {
        left: parent.left
        right: parent.right
    }
    contentHeight: thegrid.height

//    Component.onCompleted: {
//        clienthandler.getUserProfile(sender)
//    }

    Connections {
        target: clienthandler
        onUserProfile: {
            if (sender === userdata.userid) {
                displayName = page.usersinfo[sender].display_name
                avatarUrl = page.usersinfo[sender].avatar_url
                // otherInfo = userdata.other_info
            }
        }
    }

    menu: ContextMenu {
        MenuItem {
            text: qsTr("copy")
            visible: type === "m.room.message"
            onClicked: Clipboard.text = content.body
        }

        MenuItem {
            text: qsTr("event source")
            onClicked: {
                console.log("on event source clicked: " + JSON.stringify(event))
                pageStack.push(Qt.resolvedUrl("../pages/SourcePage.qml"), {eventsource: JSON.stringify(event, null, 4)})
            }
        }

        MenuItem {
            text: qsTr("cite")
            visible: type === "m.room.message"
            onClicked: {
                entertext = "> " + content.body.replace(/\r/gm, "\n> ") + "\n\n"
            }
        }

        MenuItem {
            text: qsTr("delete")
            visible: type === "m.room.message"
            onClicked: {
                pageStack.push(Qt.resolvedUrl("../pages/RedactMessage.qml"), { rid: room_id, evid: event.event_id,
                                          bd: content.body })
                eventlist.positionViewAtEnd()
            }
        }
    }

    Item {
        id: thegrid
        width: parent.width
        // columns: 2
        // spacing: 2
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: Theme.paddingMedium
        height: eventItem.height > iconColumn.height ? eventItem.height : iconColumn.height

        property bool animated: false

        Connections {
            target: clienthandler
            onMRedactEvent: {
                if (event.event_id === redactEvent.redacts) {
                    event = redactEvent
                }
            }
        }

        Column {
            id: iconColumn
            anchors.margins: Theme.paddingMedium
            width: Theme.iconSizeMedium
            // anchors.left: parent.left
            UserIcon {
                anchors.horizontalCenter: parent.horizontalCenter
                width: type === "m.room.member" ? Theme.iconSizeSmall : Theme.iconSizeMedium
                height: type === "m.room.member" ? Theme.iconSizeSmall : Theme.iconSizeMedium
                uid: usersinfo[sender].display_name
                avatarurl: usersinfo[sender].avatar_url
                idirect: true
                roomname: usersinfo[sender].display_name
                visible: lastuser !== sender
            }
            Rectangle {
                width: Theme.iconSizeMedium
                height: Theme.iconSizeMedium
                opacity: 0.0
                visible: eventlist.lname == ""
            }
        }

        Column {
            id: eventItem
            anchors.left: iconColumn.right
            anchors.margins: Theme.paddingMedium
            width: page.width - iconColumn.width - iconColumn.spacing - 4 * Theme.paddingMedium
            height: usertime.height + usertime.spacing + msgLabel.height + msgLabel.spacing
            // anchors {
                // left: iconColumn.left:S
                // right: parent.right
            // }
            Row {
                id: usertime
                width: parent.width
                anchors.right: parent.right
                anchors.left: parent.left
                height: Theme.ItemSizeExtraSmall / 2

                Label {
                    id: userLabel
                    horizontalAlignment: Text.AlignLeft
                    text: usersinfo[sender].display_name
                    color: u_color(sender)
                    visible: lastuser !== sender
                    font.bold: true
                    font.pixelSize: Theme.fontSizeExtraSmall * fontSizeConf.value
                }
                Label {
                    id: dateHour
                    width: parent.width - userLabel.width
                    horizontalAlignment: Text.AlignRight
                    text: {
                        console.log("timeer", origin_server_ts, bluepill.prettyDate(origin_server_ts))
                        return(bluepill.prettyDate( origin_server_ts))
                    }

                    color: Theme.highlightColor
                    font.pixelSize: Theme.fontSizeTiny
                }

            }
    //        Image {
    //            id: imgLabel
    //            // asynchronous: true
    //            fillMode: Image.PreserveAspectFit
    //            visible: image !== "" && !aniLabel.visible
    //            width: parent.width * (type === 'm.sticker' ? 0.3 : 1)
    //            source: image
    //        }

//            AnimatedImage {
//                id: imgLabel
//                source: image
//                fillMode: Image.PreserveAspectFit
//                visible: image !== "" && status === Image.Ready
//                width: parent.width * (type === 'm.sticker' ? 0.3 : 1)
//                playing: false
//                // autoTransform: true
//                MouseArea {
//                    anchors.fill: parent
//                    onClicked: parent.playing = !parent.playing
//                }
//            }

            Label {
                id: msgLabel
                // visible: image === ""

                anchors {
                    left: parent.left
                    right: parent.right
                }
                horizontalAlignment: type === "m.room.redaction" ? Text.AlignHCenter : Text.AlignLeft
                text: {
                    if (type === "m.room.message") {
                        if (content.format) {
                            if (content.format === 'org.matrix.custom.html') {
                                return bluepill.htmlcss + content.formatted_body
                            }
                        }
                        return content.body
                    } else if (type === "m.sticker") {
                        return ""
                    } else if (type === "m.room.encrypted") {
                        return qsTr("End to end encryption not implemented")
                    } else if (type === "m.room.redaction") {
                        return qsTr("redacted") + (content.reason !== "" ? ": " + content.reason : "")
                    } else if (type === "m.room.member") {
                            if (content.membership === "leave") {
                                return qsTr("left the room")
                            }

                            if (event.unsigned.prev_content) {
                                return qsTr("is now ") + content.displayname
                            }

                            if (content.membership === "join") {
                                return qsTr("entered the room")
                            }
                    } else {
                        return "unknown: " + type
                    }
                }
                wrapMode: Text.WordWrap
                // height: contentHeight
                width: parent.width
                font.pixelSize: Theme.fontSizeExtraSmall * fontSizeConf.value
                linkColor: Theme.highlightColor
                textFormat: Text.RichText
                onLinkActivated: Qt.openUrlExternally(link)
                Rectangle {
                    anchors.fill: parent
                    radius: height * 0.5
                    color: "grey"
                    z: -1
                    visible: type === "m.room.redaction"
                    opacity: 0.2
                }
            }
            Row {
                anchors {
                    left: parent.left
                    right: parent.right
                }
                height: Theme.iconSizeExtraSmall
            }
        }
    }
}
