#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Client handler for Bluepill
"""

import pyotherside
import threading
import sys
import os

sys.path.append("/usr/share/harbour-bluepill/python")

from bluepill.client import BluepillClientFactory
# from bluepill.testclient import BluepillClientFactory
# from bluepill.memory import Memory


def get_hostname():
    """
    Return System hostname
    """

    hostname = os.uname().nodename
    pyotherside.send("hostName", hostname)


def do_init(cachepath, sharepath):
    """
    Instanciate client and connect to the homeserver
    """

    client = BluepillClientFactory().get_client()
    client.init(cachepath, sharepath)
    client.connect()


def do_login(user, password, hostname, homeserver):
    """
    Login
    """

    client = BluepillClientFactory().get_client()
    if not client:
        pyotherside.send("No BluepillClient initiated")
    client.login(user, password, homeserver, hostname)


def get_room_list():
    """
    Get list of my rooms
    """

    client = BluepillClientFactory().get_client()
    client.get_room_list()


def get_room(room_id):
    """
    Get single room info
    """

    client = BluepillClientFactory().get_client()
    if client:
        rd = client.get_room(room_id)
        if rd:
            pyotherside.send("roomData", rd)


def get_room_info(room_id):
    BluepillClientFactory().get_client().get_room_info(room_id)


def get_bucket(room_id, bucket_id):
    BluepillClientFactory().get_client().get_bucket(room_id, bucket_id)


def get_bucket_from(room_id, bucket_id):
    BluepillClientFactory().get_client().get_bucket_from(room_id, bucket_id)


def get_room_events(room_id):
    """
    Get room events from room object
    """

    BluepillClientFactory().get_client().get_room_events(room_id)


def get_room_members(room_id):
    """
        get room events for room_id
        """

    # rd = Memory().get_object(room_id)
    # members = rd.members
    # pyotherside.send("roomMembers", room_id, members)


def join_room(room_id_or_alias: str):
    """
    Join a room
    """

    client = BluepillClientFactory().get_client()
    if client.join_room(room_id_or_alias):
        client.get_rooms()


def room_leave(room_id: str):
    """
    leave a room
    """

    BluepillClientFactory().get_client().room_leave(room_id)


def get_user_info():
    """
    Get user data of logged in user
    """

    BluepillClientFactory().get_client().get_user_info()


def get_user_profile(user_id):
    """
    Get a user profile, e. g. for displayname
    """

    BluepillClientFactory().get_client().get_user_profile(user_id)


def active_user_devices(user_id):
    """
    Get a list of active user devices for user_id
    """

    BluepillClientFactory().get_client().active_user_devices(user_id)


def send_message(room_id, message):
    """
    Send a message
    """

    BluepillClientFactory().get_client().send_message(room_id, message)
    pyotherside.send("messageSent")


def send_image(room_id, image_file):
    """
    Send an image
    """

    BluepillClientFactory().get_client().send_image(room_id, image_file)
    pyotherside.send("imageSent")


def redact_message(room_id, event_id, reason=None):
    """
    redact a message
    """

    BluepillClientFactory().get_client().redact(room_id, event_id, reason)


def seen_message(room_id):
    """
    Mark seen messages
    """

    BluepillClientFactory().get_client().seen_message(room_id)


def is_typing(room_id, timeout=30000):
    """
    User is typing
    """

    BluepillClientFactory().get_client().is_typing(room_id, timeout)


def restart_listener():
    """
    Start the listener thread (after crash?)
    """

    BluepillClientFactory().get_client().restart_listener()


def do_logout():
    """
    Logout
    """

    BluepillClientFactory().get_client().logout()


def get_unread_count(room_id):
    BluepillClientFactory().get_client().get_unread_count(room_id)


def start_key_verification(device_id):
    BluepillClientFactory().get_client().start_key_verification(device_id)


def cancel_key_verification(transaction_id):
    BluepillClientFactory().get_client().cancel_key_verification(transaction_id)


def accept_key_verification(transaction_id):
    BluepillClientFactory().get_client().accept_key_verification(transaction_id)


def confirm_key_verification(transaction_id):
    BluepillClientFactory().get_client().confirm_key_verification(transaction_id)


def verify_device(device_id, user_id=None):
    BluepillClientFactory().get_client().verify_device(device_id, user_id)


def delete_device(device_id, user_id, password):
    devices = []
    devices.append(device_id)
    BluepillClientFactory().get_client().delete_devices(devices, user_id, password)


class ClientHandler:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()

        self.bgthread1 = threading.Thread()
        self.bgthread1.start()

        self.bgthread2 = threading.Thread()
        self.bgthread2.start()

        pyotherside.atexit(self.doexit)

    def getroomlist(self):
        if self.bgthread1.is_alive():
            return
        self.bgthread1 = threading.Thread(target=get_room_list)
        self.bgthread1.start()

    def getroom(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_room, args=[room_id])
        self.bgthread.start()

    def getroomevents(self, room_id):
        if self.bgthread2.is_alive():
            return
        self.bgthread2 = threading.Thread(
            target=get_room_events, args=[room_id]
        )
        self.bgthread2.start()

    def joinroom(self, room_id_or_alias):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=join_room, args=[room_id_or_alias]
        )
        self.bgthread.start()

    def roomleave(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=room_leave, args=[room_id]
        )
        self.bgthread.start()

    def getroommembers(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=get_room_members, args=[room_id]
        )
        self.bgthread.start()

    def sendmessage(self, room_id, message):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=send_message, args=[room_id, message]
        )
        self.bgthread.start()

    def sendimage(self, room_id, image_file):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=send_image, args=[room_id, image_file]
        )
        self.bgthread.start()

    def redactmessage(self, room_id, event_id, reason=None):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=redact_message, args=[room_id, event_id, reason]
        )
        self.bgthread.start()

    def seenmessage(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=seen_message, args=[room_id])
        self.bgthread.start()

    def istyping(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=is_typing, args=[room_id])
        self.bgthread.start()

    def getunreadcount(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=get_unread_count, args=[room_id]
        )
        self.bgthread.start()

    def getuserinfo(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_user_info)
        self.bgthread.start()

    def getuserprofile(self, userid):
        if self.bgthread.is_alive():
            return
        self.bgthreat = threading.Thread(
            target=get_user_profile,
            args=[userid]
        )

    def activeuserdevices(self, userid):
        if self.bgthread.is_alive():
            return
        self.bgthreat = threading.Thread(
            target=active_user_devices,
            args=[userid]
        )

    def verifydevice(self, device_id, user_id):
        if self.bgthread.is_alive():
            return
        self.bgthreat = threading.Thread(
            target=verify_device,
            args=[device_id, user_id]
        )

    def dologout(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=do_logout)
        self.bgthread.start()

    def doexit(self):
        """
        Exit the system on leaving app
        """

        BluepillClientFactory().get_client().close()


clienthandler = ClientHandler()
