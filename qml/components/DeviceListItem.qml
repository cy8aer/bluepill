import QtQuick 2.0
import Sailfish.Silica 1.0
import QtGraphicalEffects 1.0

ListItem {
    id: displayItem
    width: parent.width
    // height: Theme.itemSizeLarge
    contentHeight: Theme.itemSizeLarge
    anchors.margins: Theme.paddingMedium

    menu: ContextMenu {
        id: contextmenu
        MenuItem {
            id: favmenu
            visible: trust_state == 'unset'
            text: qsTr('Verify with emojis')
            onClicked: {
                if(trust_state == 'unset') {
                    clienthandler.startKeyVerification(device_id)
                }
            }
        }
        MenuItem {
            id: lowmenu
            visible: trust_state == 'unset'
            text: qsTr('Text verify')
            onClicked: {
                var dialog = pageStack.push(Qt.resolvedUrl("../pages/TextVerification.qml"),
                                            {
                                                user_id: user_id,
                                                device_id: device_id,
                                                display_name: display_name,
                                                thekey: keys.ed25519
                                            })

                dialog.accepted.connect(function() {
                    console.log("Accepted")
                    clienthandler.verifyDevice(device_id, user_id)
                    clienthandler.getActiveUserDevices(user_id)
                })

                dialog.rejected.connect(function() {
                    console.log("Rejected")
                })

            }
        }
        MenuLabel {
            text: "---"
            visible: trust_state == 'unset'
        }
        MenuItem {
            text: qsTr('Delete device')
            onClicked: {
                var dialog = pageStack.push(Qt.resolvedUrl("../pages/DeleteDevice.qml"),
                                            {
                                                user_id: user_id,
                                                device_id: device_id,
                                                display_name: display_name
                                            })

                dialog.accepted.connect(function() {
                    console.log("Accepted", dialog.password)
                    clienthandler.deleteDevice(device_id, user_id, dialog.password)
                })

                dialog.rejected.connect(function() {
                    console.log("Rejected")
                })

            }
        }
    }

    Component.onCompleted: {
        // clienthandler.getUnreadCount(room_id)
    }

    Connections {
        target: clienthandler
    }


    Item {
        width: parent.width
        height: Theme.itemSizeLarge
        anchors.margins: Theme.paddingMedium
        clip: true

        Column {
            anchors.right: parent.right
            width: parent.width
            height: contentHeight

            Row {
                Icon {
                    source: trust_state === 'verified' ? "image://theme/icon-s-secure" : "image://theme/icon-s-outline-secure"
                }
                Label {
                    id: did
                    text: device_id
                    font.pixelSize: Theme.fontSizeMedium
                    font.bold: device_id == mydevice_id
                }
            }

            Label {
                id: din
                text: display_name
                font.pixelSize: Theme.fontSizeTiny
           }
        }
    }
}
