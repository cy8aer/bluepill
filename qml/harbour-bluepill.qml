import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import Nemo.Notifications 1.0
import Nemo.DBus 2.0

import "pages"
import "components"
import "cover"

ApplicationWindow
{
    id: bluepill

    property bool engineLoaded: false
    property int covstat
    property string myuser_id
    property string mydevice_id
    property string entertext

    // style stuff should be somewhere else

    property var usercolor: [ "#368bd6", "#ac3ba8", "#03b381", "#e64f7a", "#ff812d", "#2dc2c5", "#5c56f5", "#74d12c"]
    property string htmlcss: '<style>a:link { color: ' + Theme.highlightColor + '; }</style>'

    initialPage: Qt.resolvedUrl("pages/StartPage.qml")

    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations

    ConfigurationValue {
        id: favNotiValueConf
        key: "/apps/ControlPanel/bluepill/favNoti"
        defaultValue: false
    }

    ConfigurationValue {
        id: fontSizeConf
        key: "/apps/ControlPanel/bluepill/fontSize"
        defaultValue: 1.1
    }

    function prettyDate(time) {
        var date = new Date(time * 1000)
        if (time === 0)
            return (qsTr("never"))

        var diff = ((new Date()).getTime() - date.getTime()) / 1000
        var day_diff = Math.floor(diff / 86400)
        var year = date.getFullYear(),
            month = date.getMonth()+1,
            day = date.getDate();
        if (isNaN(day_diff) || day_diff < 0 || day_diff >= 31)
            return (date.toLocaleDateString())

        var r =
        (
            (
                day_diff === 0 &&
                (
                    (diff < 60 && qsTr("just now"))
                    || (diff < 120 && qsTr("1 minute ago"))
                    || (diff < 3600 && qsTr("") + Math.floor(diff / 60) + qsTr(" minutes ago"))
                    || (diff < 7200 && qsTr("1 hour ago"))
                    || (diff < 86400 && qsTr("") + Math.floor(diff / 3600) + qsTr(" hours ago"))
                )
            )
            || (day_diff === 1 && qsTr("Yesterday"))
            || (day_diff < 7 && qsTr("") + day_diff + qsTr(" days ago"))
            || (day_diff < 31 && qsTr("") + Math.ceil(day_diff / 7) + qsTr(" weeks ago"))
        );
        return r;
    }

    Timer {
        id: listenTimer
        interval: 10000
        onTriggered: {
            console.log("startListener triggered")
            clienthandler.restartTheListener()
        }
        running: false
        repeat: false
    }

    function hashCode(str) {
        var hash = 0
        var i
        var chr
        if (str.length === 0) {
            return hash
        }
        for (i = 0; i < str.length; i++) {
            chr = str.charCodeAt(i)
            hash = ((hash << 5) - hash) + chr
            hash |= 0
        }
        return Math.abs(hash);
    }

    function u_color(user_id) {
        return usercolor[hashCode(user_id) % 8]
    }

    DBusAdaptor {
        service: "harbour.bluepill.service"
        iface: "harbour.bluepill.service"
        path: "/harbour/bluepill/service"
        xml: "  <interface name=\"harbour.bluepill.service\">\n" +
             "    <method name=\"openPage\"/>\n" +
             "  </interface>\n"

        function openPage(roomid, roomname) {
            __silica_applicationwindow_instance.activate()
            pageStack.pop(null, true)
            pageStack.push(Qt.resolvedUrl("pages/RoomPage.qml"), { room_id: roomid, room_name: roomname } )
        }
    }

    Notification {
        id: logNotification
        category: "com.gitlab.cy8aer.bluepill"
        appIcon: "image://theme/icon-lock-chat"
        appName: "bluepill"
    }

    Notification {
        property string room_id
        property string room_name

        id: messageNotification
        category: "x-nemo.messaging.im"
        urgency: Notification.Critical
        // appIcon: "/usr/share/harbour-bluepill/images/q.png"
        appIcon: "image://theme/icon-lock-chat"
        appName: "bluepill"
        previewSummary: qsTr("New posts available")
        previewBody: qsTr("Click to view updates")
        body: qsTr("New Posts are available. Click to view.")

        maxContentLines: 5
        remoteActions: [ {
            name: "default",
            service: "harbour.bluepill.service",
            path: "/harbour/bluepill/service",
            iface: "harbour.bluepill.service",
            method: "openPage",
            arguments: [ room_id, room_name ]
        } ]
    }

    ClientHandlerPython {
        id: clienthandler

        onRestartListener: {
            console.log("Need to restart Listener in 10s")
            listenTimer.start()
        }

        onJoinRoomFailed: {
            var e = JSON.parse(error)
            console.log("Room joining failed", e.error)
            logNotification.previewSummary = qsTr("Room login failed")
            logNotification.previewBody = e.error
            logNotification.publish()
        }

        onKeyVerificationStart: {
            pageStack.completeAnimation()
            console.log("keyVerification starting", keydata.content.from_device)
            var dialog = pageStack.push(Qt.resolvedUrl("pages/KeyVerificationStart.qml"),
                                                        {"user_id": keydata.sender})
            dialog.accepted.connect(function() {
                console.log("Accepted")
                clienthandler.acceptKeyVerification(keydata.content.transaction_id)
            })

            dialog.rejected.connect(function() {
                console.log("Rejected")
                clienthandler.cancelKeyVerification(keydata.content.transaction_id)
            })
        }

        onKeyVerificationKey: {
            console.log("keyVerificationKey starting", device_id)
            pageStack.completeAnimation()
            var dialog = pageStack.push(Qt.resolvedUrl("pages/KeyVerificationKey.qml"),
                                        { emojis: emojis })

            dialog.accepted.connect(function() {
                console.log("Accepted")
                clienthandler.confirmKeyVerification(transaction_id)
            })

            dialog.rejected.connect(function() {
                console.log("Rejected")
                clienthandler.cancelKeyVerification(transaction_id)
            })
        }
    }
}
