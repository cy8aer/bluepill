"""
Test bluepill.singleton
"""

from bluepill.singleton import Singleton


class MyClass(metaclass=Singleton):
    """
    A test class to be a Singleton
    """

    testvar = 1


class TestClass(object):
    """
    Test class for singleton
    """

    def test_singleton(self):
        a = MyClass()
        b = MyClass()

        assert a == b
        assert b.testvar == 1

        b.testvar = 2

        assert a == b
        assert a.testvar == 2
