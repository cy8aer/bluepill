"""
client_data.py tests
"""

import pathlib
from pathlib import Path
import pyotherside

from bluepill.client_data import ClientDataFactory, ClientData
from bluepill.memory import Memory

poth_args = None


class TestClass(object):
    def test_client_data_instance(self, tmpdir, monkeypatch):
        def mockreturn(path):
            return Path(tmpdir)

        monkeypatch.setattr(
            pathlib.PosixPath, "expanduser", mockreturn, raising=True
        )

        def mockreturn2(*argc, **kwargs):
            global pyoth_args
            pyoth_args = argc

        monkeypatch.setattr(pyotherside, "send", mockreturn2)

        Memory("clientdata")

        cld1 = ClientDataFactory().clientData
        assert type(cld1) == ClientData
        cld1.homeserver = "https://example.com"
        cld1.save()

        cld2 = ClientDataFactory().clientData
        assert cld1 == cld2
        assert cld2.homeserver == "https://example.com"
