"""
test bluepill client
"""

# import pytest
# import pyotherside
# from pyotherside import send as doSend
# import bluepill
# from bluepill.interface import doSend
# from bluepill.client import BluepillClientFactory
from bluepill.client_data import ClientDataFactory
# from bluepill.client_data import ClientData

args = None

# Fake LoginError


class LoginError(BaseException):
    pass


class _Client:
    def __init__(self):
        self.teststring = "teststring"
        self._data = ClientDataFactory().client_data

    async def login(self, password):
        if password != "testpass":
            raise LoginError

        self.device_id = "device_id"
        self.user_id = "user_id"
        self.access_token = "access_token"


class TestClass(object):
    """
    Test class
    """

    # def test_mocking(self, monkeypatch):
    #     global args

    #     def mockreturn(*argc, **kwargs):
    #         global args
    #         args = argc

    #     monkeypatch.setattr(, "doSend", mockreturn)

    # doSend("test", 1)
    # assert args[0] == "test"
    # assert args[1] == 1
    # doSend("test2", 2)
    # assert args[0] == "test2"
    # assert args[1] == 2

    # def test_connect_need_login(self, monkeypatch):
    #     global args

    #     def mockreturn(*argc, **kwargs):
    #         global args
    #         args = argc

    #     def mockreturn2(homeserver, user_id):
    #         return _Client()

    #     monkeypatch.setattr(bluepill.interface, "doSend", mockreturn)
    #     monkeypatch.setattr('nio.AsyncClient', mockreturn2)

    #     client = BluepillClientFactory().get_client()
    #     client.connect()

    #     # assert args[0] == "needLogin"

    #     client._data.token = "testtoken"
    #     client._data.user_id = "@testuser:example.com"
    #     client._data.homeserver = "test.example.com"
    #     client._data.device_id = "testid"

    # def fake():
    #     pass

    # monkeypatch.setattr(client, "_start_event_loop", fake)
    # client.connect()

    # # assert client._client.teststring == "teststring"
    # assert args[0] == "initReady"
    # assert args[1] == "@testuser:example.com"

    # assert not client._data.full_state

    # def test_login(self, monkeypatch):
    #     global args

    #     def mockreturn(*argc, **kwargs):
    #         global args
    #         args = argc

    #     def mockreturn2(homeserver, user_id):
    #         return _Client()

    #     monkeypatch.setattr(pyotherside, "send", mockreturn)
    #     monkeypatch.setattr('nio.AsyncClient', mockreturn2)

    #     client = BluepillClientFactory().get_client()

    #     client.login("testuser",
    #                  "not testpass",
    #                  "https://example.com",
    #                  "https://example.com")

    #     assert args[0] == "loginError"

    #     client.login("testuser",
    #                  "testpass",
    #                  "https://example.com",
    #                  "https://example.com")

    #     assert client._data.access_token == "access_token"
    #     assert client._data.device_id == "device_id"

    #     assert client._data.full_state

    # def test_logout(self, monkeypatch):
    #     global args

    #     def mockreturn(*argc, **kwargs):
    #         global args
    #         args = argc

    #     def mockreturn2(homeserver, user_id):
    #         return _Client()

    #     monkeypatch.setattr(pyotherside, "send", mockreturn)
    #     monkeypatch.setattr('nio.AsyncClient', mockreturn2)

    #     client = BluepillClientFactory().get_client()

    #     client.login("testuser",
    #                  "testpass",
    #                  "https://example.com",
    #                  "https://example.com")

    #     assert client._data.access_token == "access_token"
    #     assert client._data.device_id == "device_id"

    #     monkeypatch.setattr('nio.AsyncClient', mockreturn2)
    #     client.logout()
    #     assert args[0] == "loggedOut"

    #     assert type(client._data) == ClientData
    #     assert client._client is None

    # def test_room_update(self):
    #     pass
