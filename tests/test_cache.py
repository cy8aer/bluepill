"""
test bluepill.cache
"""

from bluepill.cache import Cache


class TestClass(object):
    """
    test class for cache
    """

    def test_store(self):
        cache = Cache()
        len1 = cache.size
        cache.store("test1", 1)
        len2 = cache.size

        assert len1 == 0
        assert len2 == len1 + 1

    def test_delete(self):
        cache = Cache()
        cache.store("test1", 1)
        cache.store("test2", 2)
        len1 = cache.size
        cache.delete("test2")
        len2 = cache.size
        cache.delete("test2")
        len3 = cache.size

        assert len1 == 2
        assert len2 == len3 == 1

    def test_exists(self):
        cache = Cache()
        cache.store("test1", 1)
        cache.store("test2", 2)

        assert cache.exists("test1")
        assert cache.exists("test2")
        assert not cache.exists("test3")

    def test_cachelimit(self):
        cache = Cache(limit=2)
        cache.store("test1", 1)
        cache.store("test2", 2)
        cache.store("test3", 3)

        assert cache.size == 2
        assert not cache.exists("test1")
        assert cache.exists("test2")
        assert cache.exists("test3")
