"""
Bluepill client functionality
"""

# from pyotherside import send as doSend

import sys
import threading

# import os
from urllib.parse import urlparse
# from urllib.parse import quote
import re
import mimetypes
from pathlib import Path
import json

# from typing import Optional

sys.path.append("../")
sys.path.append("/usr/share/harbour-bluepill/lib/python3.8/site-packages")

import nio
import asyncio
import aiofiles
import emoji
from markdown2 import Markdown

from pyotherside import send as doSend
# from bluepill.interface import doSend
from bluepill.singleton import Singleton
from bluepill.client_data import ClientDataFactory

from bluepill.client_data import CLIENTDATA
# from bluepill.util import make_directory
from bluepill.memory import Memory


# -------------------------------------------------------

STORE_NAME = "BluepillClient"


class BluepillClient:
    """
    Bluepill Client
    """

    def __init__(self, loop=None):
        """
        Initialization of the client

        loop (loop): use an alternative loop
        """

        self._data = ClientDataFactory().clientData
        self._client = None
        self._buckets = {}
        self._bucketsize = 10

        if not loop:
            self._loop = asyncio.get_event_loop()
        else:
            self._loop = loop

        link_patterns = [
            (
                re.compile(
                    r"((([@!#]?[A-Za-z]{3,9}:(?:\/\/)?)"
                    r"(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+(:[0-9]+)?"
                    r"|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)"
                    r"((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?"
                    r"(?:[\.\!\/\\\w]*))?)"
                ),
                r"\1",
            )
        ]
        self._markdown = Markdown(
            extras=["link-patterns"],
            link_patterns=link_patterns
        )
        self.loggedin = False
        self.firstsync = True
        self._mainthread = threading.Thread()
        self._mainthread.start()

    def init(self, cache_path: str, local_path: str, bucketsize=10):
        """
        Initialization of paths.

        store_path (str): Path to store key database
        """

        self._cache_path = Path(cache_path)
        self._local_path = Path(local_path)
        self._store_path = self._local_path
        self._bucketsize = bucketsize

        self._cache_path.mkdir(mode=0o755, parents=True, exist_ok=True)
        self._local_path.mkdir(mode=0o755, parents=True, exist_ok=True)

    def connect(self) -> None:
        """
        Connect to the matrix server.
        - If no token exists, signal needLogin
        - If token exists startup event loop and signal initReady
        """

        if not self._data.access_token:
            doSend("needLogin")
            return

        config = nio.ClientConfig(store_sync_tokens=True)
        self._client = nio.AsyncClient(
            self._data.homeserver,
            self._data.user_id,
            store_path=self._store_path,
            config=config,
        )

        self._client.access_token = self._data.access_token
        self._client.device_id = self._data.device_id
        self._client.user_id = self._data.user_id
        self._client.rooms = self._data.rooms  # for offline scenario?

        self.firstsync = True

        async def do_connect():
            self._client.load_store()
            doSend("initReady", self._data.user_id, self._data.device_id)
            await self._start_event_loop()

        def threadstart():
            try:
                self._loop.run_until_complete(do_connect())
            except:
                doSend(
                    "errorMessage",
                    "Could not start main loop"
                )

        if self._mainthread.is_alive():
            return

        self._mainthread = threading.Thread(target=threadstart)
        self._mainthread.start()

    def login(
        self, user: str, password: str, homeserver: str, device_name: str = ""
    ) -> None:
        """
        Login to the server. Will initialize token
        Args:
            user (str): the user name to be logged in
            password (str): the password to login with
            device_name(str): A display name to assign to a newly-created
                              device.
            homeserver (str): the Matrix homeserver
        """

        async def do_login(homeserver, user, password):
            config = nio.ClientConfig(store_sync_tokens=True)
            self._client = nio.AsyncClient(
                homeserver, user, store_path=self._store_path, config=config
            )
            try:
                await self._client.login(password, device_name=device_name)
            except Exception as arg:
                doSend("loginError", "{0}".format(arg))
                return

            if not self._client.logged_in:
                doSend("loginError", "Login failed")
                return

            doSend("Token", self._client.access_token)  # not needed
            self._data.next_batch = None
            self._data.homeserver = homeserver
            self._data.user_id = self._client.user_id
            self._data.device_id = self._client.device_id
            self._data.device_name = device_name
            self._data.access_token = self._client.access_token
            self._data.save()

            doSend("loggedIn", self._data.user_id)
            self.loggedin = True
            await self._start_event_loop()

        def threadstart(homeserver, user, password):
            try:
                self._loop.run_until_complete(
                    do_login(
                        homeserver, user, password
                    )
                )
            except:
                pass

        if self._mainthread.is_alive():
            return
        self._mainthread = threading.Thread(
            target=threadstart, args=[homeserver, user, password]
        )

        self._mainthread.start()

    def logout(self):
        """
        Log system out
        """

        try:
            self._loop.run_until_complete(
                self._client.logout(self._data.access_token, all_devices=True)
            )
        except:
            doSend("logoutError")

        self._client = None
        ClientDataFactory().client_data = None
        Memory().delete_object(CLIENTDATA)
        self._data = ClientDataFactory().clientData
        self._data.access_token = None
        self._data.save()
        doSend("loggedOut")

    def close(self):
        """
        Close the device: shutdown asyncio loop
        """

        if self._data.access_token:
            self._data.rooms = self._client.rooms
            self._data.save()

        asyncio.run_coroutine_threadsafe(
            self._loop.shutdown_asyncgens(),
            self._loop
        )

        try:
            self._loop.stop()
            self._loop.close()
        except:
            pass

    async def _start_event_loop(self):
        """
        Start the event loop thread
        """

        event_callbacks = [
            (self._message_text_cb, nio.RoomMessageText),
            (self._message_emote_cb, nio.RoomMessageEmote),
            (self._message_notice_cb, nio.RoomMessageNotice),
            (self._message_image_cb, nio.RoomMessageImage),
            (self._message_video_cb, nio.RoomMessageVideo),
            (self._message_file_cb, nio.RoomMessageFile),
            (self._tag_event_cb, nio.account_data.TagEvent),
            (self._room_member_event_cb, nio.RoomMemberEvent),
            (self._invite_member_cb, nio.InviteEvent),
            (self._unknown_event_cb, nio.UnknownEvent)
        ]

        ephemeral_callbacks = [
            (self._ephemeral_receipt_cb, nio.ReceiptEvent),
            (self._ephemeral_typingnotice_cb, nio.TypingNoticeEvent)
        ]

        response_callbacks = [
            (self._response_synced_cb, nio.responses.SyncResponse),
            (self._response_sync_error_cb, nio.responses.SyncError)
        ]

        to_device_callbacks = [
            (self._to_device_event_cb, nio.ToDeviceEvent),
            (self._key_verification_start_cb, nio.KeyVerificationStart),
            (self._key_verification_accept_cb, nio.KeyVerificationAccept),
            (self._key_verification_key_cb, nio.KeyVerificationKey),
            (self._key_verification_mac_cb, nio.KeyVerificationMac),
            (self._key_verification_cancel_cb, nio.KeyVerificationCancel),
            (self._base_room_key_request_cb, nio.BaseRoomKeyRequest)
        ]

        for cbs in event_callbacks:
            self._client.add_event_callback(cbs[0], cbs[1])

        for cbs in ephemeral_callbacks:
            self._client.add_ephemeral_callback(cbs[0], cbs[1])

        for cbs in response_callbacks:
            self._client.add_response_callback(cbs[0], cbs[1])

        for cbs in to_device_callbacks:
            self._client.add_to_device_callback(cbs[0], cbs[1])

        self._client.add_room_account_data_callback(
            self._room_account_data_cb,
            nio.TagEvent
        )

        await self._client.sync_forever(timeout=30000, full_state=self.firstsync)

    async def _room_account_data_cb(self, room, event):
        room_prio = ""
        if 'm.favourite' in event.tags:
            room_prio = "fav"
        elif 'm.lowpriority' in event.tags:
            room_prio = "low"

        doSend("roomTagChanged", room.room_id, room_prio)

    async def _message_text_cb(self, room, event):
        event.source["origin_server_ts"] = event.source["origin_server_ts"] / 1000.
        event.body = emoji.emojize(event.body, use_aliases=True)
        event.formatted_body = self._markdown.convert(event.body)
        self._hot_append(room.room_id, event.source)
        doSend(
            "roomMessageText",
            room.room_id,
            room.user_name(event.sender),
            event.source,
            event.verified,
            event.decrypted
        )
        doSend("newMessage")

    async def _message_emote_cb(self, room, event):
        event.source["origin_server_ts"] = event.source["origin_server_ts"] / 1000.
        self._hot_append(room.room_id, event.source)
        doSend(
            "roomMessageEmote", room.room_id, room.user_name(event.sender), event.source
        )
        doSend("newMessage")

    async def _message_notice_cb(self, room, event):
        event.source["origin_server_ts"] = event.source["origin_server_ts"] / 1000.
        self._hot_append(room.room_id, event.source)
        doSend(
            "roomMessageNotice",
            room.room_id,
            room.user_name(event.sender),
            event.source,
        )
        doSend("newMessage")

    async def _message_image_cb(self, room, event):
        event.source["origin_server_ts"] = event.source["origin_server_ts"] / 1000.
        self._hot_append(room.room_id, event.source)
        doSend(
            "roomMessageImage", room.room_id, room.user_name(event.sender), event.source
        )
        doSend("newMessage")

    async def _message_audio_cb(self, room, event):
        event.source["origin_server_ts"] = event.source["origin_server_ts"] / 1000.
        self._hot_append(room.room_id, event.source)
        doSend(
            "roomMessageAudio", room.room_id, room.user_name(event.sender), event.source
        )

    async def _message_video_cb(self, room, event):
        event.source["origin_server_ts"] = event.source["origin_server_ts"] / 1000.
        self._hot_append(room.room_id, event.source)
        doSend(
            "roomMessageVideo", room.room_id, room.user_name(event.sender), event.source
        )
        doSend("newMessage")

    async def _message_file_cb(self, room, event):
        event.source["origin_server_ts"] = event.source["origin_server_ts"] / 1000.
        self._hot_append(room.room_id, event.source)
        doSend(
            "roomMessageFile", room.room_id, room.user_name(event.sender), event.source
        )
        doSend("newMessage")

    async def _ephemeral_typingnotice_cb(self, room, event):
        #     event.server_timestamp = int(event.server_timestamp / 1000)
        doSend("typingNotice", room.room_id, event.users)

    async def _ephemeral_receipt_cb(self, room, event):
        doSend(
            "receiptEvent",
            room.room_id,
            event
        )

    async def _to_device_event_cb(self, event):
        doSend("toDeviceEvent", event)

    async def _key_verification_start_cb(self, event):
        doSend("keyVerificationStart", event.source)

    async def _key_verification_accept_cb(self, event):
        doSend("keyVerificationAccept", event)

    async def _key_verification_key_cb(self, event):
        sas = self._client.key_verifications.get(event.transaction_id)
        device = sas.other_olm_device
        emoji = sas.get_emoji()

        doSend(
            "keyVerificationKey",
            self._client.user_id,
            device.user_id,
            device.id,
            sas.transaction_id,
            emoji
        )

    async def _key_verification_mac_cb(self, event):
        sas = self._client.key_verifications.get(event.transaction_id)
        if not sas:
            return
        device = sas.other_olm_device
        if sas.verified:
            doSend(
                "keyVerificationMac",
                self._client.user_id,
                device.user_id,
                device.id,
                sas.transaction_id
            )

    async def _key_verification_cancel_cb(self, event):
        doSend("keyVerficationCancel", event)

    async def _base_room_key_request_cb(self, event):
        doSend("baseRoomKeyRequest", event)

    async def _response_synced_cb(self, response):
        if self.loggedin:
            self.loggedin = False
            self._data.next_batch = self._client.next_batch
            doSend("initReady", self._data.user_id, self._data.device_id)

        if self._data.rooms != self._client.rooms:
            self._data.rooms = self._client.rooms
            self._data.save()

        self.firstsync = False

    async def _tag_event_cb(self, response):
        doSend("tagEvent")

    async def _invite_member_cb(self, *args):
        doSend("inviteEvent", len(args))

    async def _room_create_cb(self, room, event):
        doSend("roomCreate", room.room_id)

    async def _unknown_event_cb(self, room_id, event):
        doSend("UnknownEvent", room_id, event)

    async def _room_member_event_cb(self, room, event):
        doSend("roomMemberEvent", room.room_id, event)

    async def _response_sync_error_cb(self, response):
        doSend("syncErrorResponse", response.message)

    def room_messages(
            self,
            room_id: str,
            start: str,
            limit: int = 0
    ):
        """
        Get room messages
        Args:
        room_id (str): Id of room
        start (str): start position id (or None)
        limit (int): limit of events (default self._bucketsize)
        """

        if limit == 0:
            limit = self._bucketsize

        return asyncio.run_coroutine_threadsafe(
            self._client.room_messages(
                room_id, start, "", nio.MessageDirection.back, limit
            ),
            self._loop,
        ).result()

    def joined_rooms(self):
        res = asyncio.run_coroutine_threadsafe(
            self._client.joined_rooms(),
            self._loop
        ).result()
        try:
            rooms = res.rooms
        except:
            return

        # for room in [
        #         room
        #         for room in self._client.rooms
        #         if room not in rooms
        # ]:
        #     r = self._client.rooms.pop(room)
        #     doSend("roomLeft", r.room_id)
        return rooms

    def joined_members(self, room_id):
        res = asyncio.run_coroutine_threadsafe(
            self._client.joined_members(room_id),
            self._loop
        ).result()
        doSend("joinedMembers", room_id, res)
        return res

    def get_joined_rooms(self):
        doSend("joinedRooms", self.joined_rooms())

    def room_get_state(self, room_id):
        return asyncio.run_coroutine_threadsafe(
            self._client.room_get_state(room_id),
            self._loop
        ).result()

    def room_read_markers(self, room_id, fully_read_event):
        return asyncio.run_coroutine_threadsafe(
            self._client.room_read_markers(room_id, fully_read_event),
            self._loop
        ).result()

    def room_typing(self, room_id, typing_state, timeout=30000):
        return asyncio.run_coroutine_threadsafe(
            self._client.room_typing(room_id, typing_state, timeout),
            self._loop
        ).result()

    def get_profile(self, user):
        return asyncio.run_coroutine_threadsafe(
            self._client.get_profile(user),
            self._loop
        ).result()

    def room_list(self):
        room_data = {}
        for room_id in self._client.rooms.keys():
            room = self._client.rooms[room_id]

            prio = ""
            if 'm.favourite' in room.tags:
                prio = "fav"
            elif 'm.lowpriority' in room.tags:
                prio = "low"

            room_data[room_id] = {
                'name': room.name,
                'display_name': room.display_name,
                'summary': room.summary,
                'topic': room.topic,
                'avatar_url': room.gen_avatar_url,
                'encrypted': room.encrypted,
                'is_group': room.is_group,
                'priority': prio
            }
        return room_data

    def get_rooms(self):
        doSend("roomsList", self.room_list())

    def get_room_info(self, room_id: str):
        """
        Return user names and avatar_urls,
        """

        users = {}
        if not self._data.room_infos:
            self._data.room_infos = {}

        if room_id in self._data.room_infos:
            users = self._data.room_infos[room_id]
        else:
            room = self._client.rooms[room_id]
            for user in room.users:
                u = room.users[user]
                av = u.avatar_url
                if av != '':
                    avatar_url = asyncio.run_coroutine_threadsafe(
                        self.download(av),
                        self._loop
                    ).result()
                else:
                    avatar_url = None

                users[user] = {
                    "display_name": u.display_name,
                    "avatar_url": avatar_url
                }
            self._data.room_infos[room_id] = users
            self._data.save()

        doSend("roomInfo", users)

    def get_devices(self):
        """
        Get user's devices
        """

        return asyncio.run_coroutine_threadsafe(
            self._client.devices(), self._loop
        ).result()

    def delete_devices(self, devices, user, password):
        """
        Delete a device
        """

        auth = {
            "type": "m.login.password",
            "user": user,
            "password": password
        }
        res = asyncio.run_coroutine_threadsafe(
            self._client.delete_devices(
                devices,
                auth
            ),
            self._loop
        )
        doSend(type(res))
        if type(res.result()) == nio.responses.DeleteDevicesResponse:
            doSend("devicesDeleted", devices)
        elif type(res.result()) == nio.responses.DeleteDevicesError:
            doSend("devicesDeletionFailed", devices)
        elif type(res.result()) == nio.responses.DeleteDevicesAuthResponse:
            doSend("devicesDeletionAuthFailed", devices)
        return res

    def active_user_devices(self, user_id):
        devices = [
            i.as_dict()
            for i in self._client.device_store.active_user_devices(user_id)
        ]
        doSend('activeUserDevices', devices)
        return devices

    async def _savefile(self, filename, data):
        async with aiofiles.open(filename, mode="wb") as f:
            f.write(data)

    async def _loadjson(self, filename):
        async with aiofiles.open(filename, "r") as bk:
            data = json.load(bk)
        return data

    async def _savejson(self, filename, data):
        async with aiofiles.open(filename, "w") as bk:
            json.dump(data, bk)

    async def download(self, url, load_again=False):
        """
        Download mxc url
        """

        o = urlparse(url)
        server_name = o.netloc
        media_id = o.path[1:]

        if not load_again:
            if media_id in self._data.media:
                return self._data.media[media_id].as_posix()

        try:
            dwn = await self._client.download(
                server_name,
                media_id
            )
        except:
            doSend("downloadError", "unknown")
            return ""

        if type(dwn) == nio.responses.DownloadError:
            doSend("downloadError", dwn.message)
            return ""

        if not dwn.filename:
            filename = media_id + mimetypes.guess_extension(dwn.content_type)
        else:
            filename = dwn.filename

        mediapath = self._cache_path / "media"
        mediapath.mkdir(parents=True, exist_ok=True)

        filename = mediapath / filename

        try:
            with open(filename, mode="wb") as f:
                f.write(dwn.body)
        except:
            return ""

        self._data.media[media_id] = filename
        self._data.save()

        return filename.as_posix()

    def thumbnail(self, url, width, height, load_again=False):
        """
        Download mxc url
        """

        o = urlparse(url)
        server_name = o.netloc
        media_id = o.path[1:]

        if not load_again:
            if media_id in self._data.media:
                return self._data.media[media_id].as_posix()

        try:
            dwn = asyncio.run_coroutine_threadsafe(
                self._client.thumbnail(
                    server_name,
                    media_id,
                    width,
                    height
                ),
                self._loop
            ).result()
        except:
            doSend("downloadError", "unknown")
            return ""

        if type(dwn) == nio.responses.ThumbnailError:
            doSend("downloadError", dwn.message)
            return ""
        if not dwn.filename:
            filename = media_id + mimetypes.guess_extension(dwn.content_type)
        else:
            filename = dwn.filename

        with aiofiles.open(filename, "w") as bk:
            bk.write(dwn.body)

        # asyncio.run_coroutine_threadsafe(
        #     self._savefile(filename, dwn.body),
        #     self._loop
        # )

        self._data.media[media_id] = filename
        self._data.save()

        return filename.as_posix()

    def room_forget(self, room_id):
        ret = asyncio.run_coroutine_threadsafe(
            self._client.room_forget(room_id),
            self._loop
        ).result()

        if type(ret) == nio.responses.RoomForgetError:
            doSend("roomForgetError", ret.message)
        elif type(ret) == nio.responses.RoomForgetResponse:
            doSend("roomForgetResponse", room_id)

    def room_leave(self, room_id):
        ret = asyncio.run_coroutine_threadsafe(
            self._client.room_leave(room_id),
            self._loop
        ).result()

        if type(ret) == nio.responses.RoomLeaveError:
            doSend("roomLeaveError", ret.message)
        elif type(ret) == nio.responses.RoomLeaveResponse:
            doSend("roomLeaveResponse", room_id)

    def room_send(self, room_id, message_type, content):
        ret = asyncio.run_coroutine_threadsafe(
            self._client.room_send(
                room_id,
                message_type,
                content
            ),
            self._loop
        ).result()
        return ret

    def room_send_text(self, room_id, body):
        body = emoji.emojize(body, use_aliases=True)
        formatted_body = self._markdown.convert(body)

        self.room_send(
            room_id,
            "m.room.message",
            {
                "msgtype": "m.text",
                "body": body,
                "formatted_body": formatted_body
            }
        )

    # Buckets

    def bucket(self, room_id: str, bucket_id: str):
        """
        get a bucket
        Args:
            room_id(str): the room to get the bucket from
            bucket_id(str): the id of the bucket
                            (this is the stop batch number)
        Returns: bucket if exists or None
        """

        if room_id in self._buckets:
            roombuckets = self._buckets[room_id]
        else:
            roombuckets = {
                "room_id": room_id,
                "buckets": []
            }
            self._buckets[room_id] = roombuckets

        if bucket_id in roombuckets:
            return roombuckets[bucket_id]

        roompath = self._cache_path / "buckets" / room_id
        roompath.mkdir(parents=True, exist_ok=True)
        bucketpath = roompath / bucket_id
        if bucketpath.exists():
            # bucket = asyncio.run_coroutine_threadsafe(
            #     self._loadjson(bucketpath),
            #     self._loop
            # )

            with open(bucketpath, "r") as bk:
                bucket = json.load(bk)

            roombuckets[bucket_id] = bucket
            return bucket

        if bucket_id == 'hot_bucket':
            bucket = {
                "start": "",
                "end": self._client.next_batch,
                "events": [],
                "forward": True,
                "id": 'hot_bucket'
            }

        else:
            resp = self.room_messages(
                room_id,
                bucket_id,
                limit=10
            )

            if type(resp) == nio.RoomMessagesError:
                return None

            bucket = {}
            bucket["start"] = resp.start
            bucket["end"] = resp.end
            bucket["events"] = []
            bucket["forward"] = False
            bucket["id"] = resp.start
            chunk = resp.chunk

            for event in chunk:
                s = event.source
                s["origin_server_ts"] = s["origin_server_ts"] / 1000.
                bucket["events"].append(event.source)

        self._save_bucket(bucketpath, bucket)
        roombuckets[bucket_id] = bucket
        return bucket

    def get_bucket(self, room_id, bucket_id):
        doSend(
            "bucketGet",
            room_id,
            bucket_id,
            self.bucket(room_id, bucket_id)
        )

    def last_bucket_from(self, room_id, bucket_id):
        bucket = self.bucket(room_id, bucket_id)
        if bucket["forward"] is True:
            s = "start"
            e = "end"
        else:
            s = "end"
            e = "start"

        if bucket[s] == "":
            resp = self.room_messages(
                room_id,
                bucket[e]
            )
            if type(resp) == nio.RoomMessagesError:
                return None

            bucket[s] = resp.end
            bucket['id'] = resp.end

            roompath = self._cache_path / "buckets" / room_id
            roompath.mkdir(parents=True, exist_ok=True)
            bucketpath = roompath / bucket_id
            self._save_bucket(bucketpath, bucket)
        return self.bucket(room_id, bucket[s])

    def get_bucket_from(self, room_id, bucket_id):
        doSend(
            "bucketGetFrom",
            room_id,
            bucket_id,
            self.last_bucket_from(room_id, bucket_id)
        )

    def get_last_event(self, room_id):
        bucket = self.bucket(room_id, 'hot_bucket')
        if len(bucket["events"]) < 1:
            return None
        else:
            return bucket["events"][len(bucket["events"]) - 1]

    def _hot_append(self, room_id, event):
        nb = self._client.next_batch
        roompath = self._cache_path / "buckets" / room_id
        roompath.mkdir(parents=True, exist_ok=True)

        hot_bucket = self.bucket(room_id, 'hot_bucket')
        if len(hot_bucket["events"]) >= self._bucketsize:
            lb = hot_bucket["end"]
            hot_bucket['id'] = lb
            bucketpath = roompath / lb
            self._save_bucket(bucketpath, hot_bucket)
            hot_bucket = {
                "start": lb,
                "end": nb,
                "events": [],
                "forward": True,
                "id": 'hot_bucket'
            }

        hot_bucket["events"].append(event)
        hot_bucket["end"] = nb

        bucketpath = roompath / 'hot_bucket'
        self._save_bucket(bucketpath, hot_bucket)
        roombuckets = self._buckets[room_id]
        roombuckets['hot_bucket'] = hot_bucket

    def _save_bucket(self, bucketpath, bucket):
        # asyncio.run_coroutine_threadsafe(
        #     self._savejson(bucketpath, bucket),
        #     self._loop
        # )

        with open(bucketpath, "w") as bk:
            json.dump(bucket, bk)

    # Crypto stuff

    def verify_device(self, device_id, user_id=None):
        if not user_id:
            user_id = self._client.user_id
        device = self._client.device_store[user_id][device_id]
        ret = self._client.verify_device(device)
        doSend("deviceVerified", device_id)
        return ret

    def start_key_verification(self, device_id, user_id=None):
        if not user_id:
            user_id = self._client.user_id
        device = self._client.device_store[self._client.user_id][device_id]
        return asyncio.run_coroutine_threadsafe(
            self._client.start_key_verification(device),
            self._loop
        ).result()

    def accept_key_verification(self, transaction_id):
        return asyncio.run_coroutine_threadsafe(
            self._client.accept_key_verification(transaction_id),
            self._loop
        ).result()

    def confirm_key_verification(self, transaction_id):
        res = asyncio.run_coroutine_threadsafe(
            self._client.confirm_short_auth_string(transaction_id),
            self._loop
        ).result()
        doSend(
            'confirmedKeyVerification',
            transaction_id
        )
        return res

    def cancel_key_verification(self, transaction_id, reject=False):
        res = asyncio.run_coroutine_threadsafe(
            self._client.cancel_key_verification(
                transaction_id,
                reject=reject
            ),
            self._loop
        ).result()
        doSend(
            'keyVerificationCanceled',
            transaction_id
        )
        return res

    def export_keys(self, outfile, passphrase):
        asyncio.run_coroutine_threadsafe(
            self._client.export_keys(outfile, passphrase),
            self._loop
        ).result()

    def import_keys(self, infile, passphrase):
        asyncio.run_coroutine_threadsafe(
            self._client.import_keys(infile, passphrase),
            self._loop
        )

    # Utils

    def get_emojis_from_str(self, emofrac: str):
        """
        Send a list of emojis depending on emofrac (fraction of
        an emoji name)
        Args:
            emofrac(str): fraction of an emoji name
        """

        emojis = {}
        for val in (val for val in emoji.UNICODE_EMOJI.values() if emofrac in val):
            key = list(emoji.UNICODE_EMOJI.keys())[
                list(emoji.UNICODE_EMOJI.values()).index(val)
            ]
            emojis[val] = key
        doSend("emojiFrac", emojis)

    def _lineparser(self, line):
        """
        Parse the line and react on "/" in first row
        """

        pass
        # commands = {
        #     "me": "showme",
        #     "shrug": "showshrug",
        #     "lenny": "showlenny",
        #     "plain": "doplain",
        #     "html": "dohtml"
        # }
        # GUI communication stuff

    def get_room_list(self):
        """
        Send a dict of all joined rooms with room information
        """

        roomlist = []
        for room_id in self._client.rooms.keys():
            room = self._client.rooms[room_id]
            prio = ""
            if 'm.favourite' in room.tags:
                prio = "fav"
            elif 'm.lowpriority' in room.tags:
                prio = "low"

            rd = {}
            event = self.get_last_event(room_id)
            if room.gen_avatar_url:
                avatar_url = asyncio.run_coroutine_threadsafe(
                    self.download(room.gen_avatar_url),
                    self._loop
                ).result()
            else:
                avatar_url = ""

            if event:
                rd["event"] = event
                rd["origin_server_ts"] = event["origin_server_ts"]
                rd["sender_name"] = room.user_name(event["sender"])
            else:
                rd["event"] = None
                rd["origin_server_ts"] = 0
                rd["sender_name"] = ""

            rd["room_id"] = room_id
            rd["display_name"] = room.display_name
            rd["topic"] = room.topic
            rd["avatar_url"] = avatar_url
            rd["encrypted"] = room.encrypted
            rd['is_group'] = room.is_group
            rd['priority'] = prio
            roomlist.append(rd)

        roomlist.sort(key=lambda r: r["origin_server_ts"], reverse=True)
        doSend("roomList", roomlist)
        return roomlist

    def get_user(self, room_id, user_id):
        room = self._client.rooms[room_id]
        user = room.users[user_id]
        if user.avatar_url:
            avatar_url = asyncio.run_coroutine_threadsafe(
                self.download(user.avatar_url),
                self._loop
            ).result()
        else:
            avatar_url = ""

        doSend("userData", {
            "display_name": user.display_name,
            "avatar_url": avatar_url,
            "power_level": user.power_level
        })

    def get_user_presence(self, user_id):
        ret = asyncio.run_coroutine_threadsafe(
            self._client.get_presence(user_id),
            self._loop
        ).result()
        doSend("userPresence", ret)
        return ret

    def get_user_profile(self, user_id):
        if user_id in self._data.users:
            doSend("userProfile", self._data.users[user_id])

        ret = asyncio.run_coroutine_threadsafe(
            self._client.get_profile(user_id),
            self._loop
        ).result()
        try:
            if ret.avatar_url:
                avatar_url = self.download(ret.avatar_url)
            else:
                avatar_url = ""
        except:
            avatar_url = ""

        userdata = {
            'userid': user_id,
            'displayname': ret.displayname,
            'avatar_url': avatar_url,
            'other_info': ret.other_info
        }

        self._data.users[user_id] = userdata
        self._data.save()
        doSend("userProfile", userdata)
        # return ret

    # def room_update(self, response):
    #     """
    #     Update room information list
    #     """

    #     invite = response.rooms.invite
    #     # joined rooms are interpreted in self._client.rooms
    #     leave = response.rooms.leave

    #     for lroom in leave:
    #         # delete all rooms in leave from room list
    #         pass

    #     for room in self._client.rooms:
    #         # now set actual data to joined rooms
    #         # rd = RoomDataFactory().room_data(room.room_id)
    #         pass

    #     for iroom in invite:
    #         # we need to send messages for invited rooms
    #         pass

    # def sync(self):
    #     """
    #     Sync process
    #     """

    #     try:
    #         resp = self._loop.run_until_complete(
    #             self._client.sync(
    #                 since=self._data.next_batch,
    #                 full_state=self._data.full_state,
    #             )
    #         )
    #     except SyncError:
    #         pass

    #     self.room_update(resp)

    # def set_listeners(self):
    #     """
    #     set listeners for event loop
    #     """

    # Startup Event loop

    # # ----------------------------------------- oldstuff

    # # room_update(response)
    # # event_update()
    # # set_listeners()

    # pyotherside.send("isConnected")

    # pyotherside.send("login")
    # try:
    #     self._client = MatrixClient(homeserver)
    #     self._data.token = self._client.login(
    #         user, password, limit=100, sync=True
    #     )
    #     pyotherside.send("gotToken", self._data.token)
    # except MatrixRequestError:
    #     pyotherside.send("loginFailed")
    #     return

    # pyotherside.send("loggedIn")

    # self._data.encryption = True
    # user = self._client.get_user(self._data.user_id)
    # self._data.user_name = user.get_display_name()
    # avatar_url = user.get_avatar_url()
    # self._data.user_avatar_url = (
    #     self._get_url(avatar_url) if avatar_url else ""
    # )

    # response = self._client.api.sync()

    # self.get_room_dir(response)
    # self.get_rooms()
    # self._data.save()

    # self.start_room_event_listener()
    # self.start_listener()

    # pyotherside.send("initReady", self._data.user_id)
    # pyotherside.send("log", "token", self._data.token)

    # pyotherside.send("initReady", self._data.user_id)

    # Startup Event loop

    # # room_update(response)
    # # event_update()
    # # set_listeners()

    # pyotherside.send("isConnected")

    # ############################# old

    # pyotherside.send("log", "token", self._data.token)
    # try:
    #     self._client = MatrixClient(
    #         self._data.homeserver,
    #         user_id=self._data.user_id,
    #         token=self._data.token,
    #     )
    # except:
    #     pyotherside.send("connectFailed")
    #     self._client = None
    #     pyotherside.send("initReady", self._data.user_id)
    #     return

    # pyotherside.send("isConnected")

    # self.sync()
    # self.sync_rooms()
    # self.start_room_event_listener()
    # self.start_listener()

    # def do_exit(self):
    #     """
    #     Stop the listener Threads
    #     """

    #     self.stop_listener()
    #     self.stop_room_event_listener()
    #     self.save()

    # def join_room(self, room_id_or_alias: str) -> bool:
    #     """
    #     Join the room
    #     """

    #     try:
    #         room = self._client.join_room(room_id_or_alias)
    #     except MatrixRequestError as e:
    #         pyotherside.send("joinRoomFailed", e.content)
    #         return False
    #     if not room:
    #         return False

    #     self._create_room(room_id=None, rd=room)
    #     self._data.roomdir.append(room.room_id)
    #     self._data.save()
    #     return True

    # def get_room_dir(self, response):
    #     """
    #     Get direct rooms from account_data
    #     """

    #     account_data = response["account_data"]
    #     event = next(
    #         filter(lambda x: x["type"] == "m.direct", account_data["events"]),
    #         None,
    #     )
    #     if event:
    #         pyotherside.send("log", "m.direct in account_data")
    #         self._data.roomdir = [
    #             j for i in event["content"].values() for j in i
    #         ]

    # def format_event(self, room_id: str, event):
    #     """
    #     TODO: event.py?
    #     """

    #     image = ""
    #     user_id = event["sender"]
    #     user = self._client.get_user(user_id)
    #     try:
    #         avatar_url = user.get_avatar_url()
    #     except:
    #         avatar_url = ""
    #     avatar_url = self._get_url(avatar_url) if avatar_url else ""
    #     dat = event["origin_server_ts"] / 1000
    #     event["origin_server_ts"] = dat
    #     try:
    #         name = user.get_friendly_name()
    #     except:
    #         name = ""
    #     try:
    #         if event["content"]["msgtype"] == "m.image":
    #             image = event["content"]["url"]
    #             pyotherside.send("image: ", image)
    #             image = (
    #                 self._get_url(self._client.api.get_download_url(image))
    #                 if image
    #                 else ""
    #             )
    #     except:
    #         image = ""

    #     try:
    #         if event["type"] == "m.sticker":
    #             image = event["content"]["url"]
    #             if image:
    #                 image = self._get_url(
    #                     self._client.api.get_download_url(image)
    #                 )
    #     except:
    #         image = ""

    #     try:
    #         if event["content"]["msgtype"] == "m.text":
    #             event["content"]["body"] = emoji.emojize(
    #                 event["content"]["body"], use_aliases=True
    #             )
    #             event["content"]["formatted_body"] = self._markdown.convert(
    #                 event["content"]["body"]
    #             )
    #             event["content"]["format"] = "org.matrix.custom.html"
    #     except:
    #         pass
    #     room_data = RoomDataFactory().room_data(room_id)
    #     if room_data:
    #         room_name = room_data.display_name
    #     else:
    #         room_name = ""

    #     asect = util.format_full_date(dat)
    #     return (
    #         dat,
    #         {
    #             "event": event,
    #             "name": name,
    #             "rname": room_name,
    #             "user_id": user_id,
    #             "edate": dat,
    #             "avatar_url": avatar_url,
    #             "image": image,
    #             "asect": asect,
    #         },
    #     )

    # def room_append_event(
    #     self, room_id: Optional[str], event, save: bool = True
    # ):
    #     """
    #     Append a room event
    #     TODO: to room.py
    #     """

    #     if not room_id:
    #         return None
    #     room_data = RoomDataFactory().room_data(room_id)
    #     if not room_data:
    #         return None
    #     room_name = room_data.display_name
    #     room_level = room_data.level

    #     image = ""
    #     user_id = event["sender"]
    #     user = self._client.get_user(user_id)
    #     try:
    #         avatar_url = user.get_avatar_url()
    #     except:
    #         avatar_url = ""
    #     avatar_url = self._get_url(avatar_url) if avatar_url else ""
    #     dat = event["origin_server_ts"] / 1000
    #     event["origin_server_ts"] = dat
    #     try:
    #         name = user.get_friendly_name()
    #     except:
    #         name = ""
    #     try:
    #         if event["content"]["msgtype"] == "m.image":
    #             image = event["content"]["url"]
    #             pyotherside.send("image: ", image)
    #             image = (
    #                 self._get_url(self._client.api.get_download_url(image))
    #                 if image
    #                 else ""
    #             )
    #     except:
    #         image = ""

    #     try:
    #         if event["type"] == "m.sticker":
    #             image = event["content"]["url"]
    #             if image:
    #                 image = self._get_url(
    #                     self._client.api.get_download_url(image)
    #                 )
    #     except:
    #         image = ""

    #     try:
    #         if event["content"]["msgtype"] == "m.text":
    #             event["content"]["body"] = emoji.emojize(
    #                 event["content"]["body"], use_aliases=True
    #             )
    #             event["content"]["formatted_body"] = self._markdown.convert(
    #                 event["content"]["body"]
    #             )
    #             event["content"]["format"] = "org.matrix.custom.html"
    #     except:
    #         pass

    #     asect = util.format_full_date(dat)

    #     event_data = {
    #         "event": event,
    #         "name": name,
    #         "rname": room_name,
    #         "rlevel": room_level,
    #         "user_id": user_id,
    #         "edate": dat,
    #         "avatar_url": avatar_url,
    #         "image": image,
    #         "asect": asect,
    #     }
    #     room_data.events.append(event_data)
    #     room_data.last_time = dat
    #     if save:
    #         room_data.end_token = self._client.api.get_room_messages(
    #             room_id, None, "f"
    #         )["end"]
    #         room_data.save()
    #     return event_data

    # def _create_room(self, room_id: str = None, rd=None):
    #     """
    #     Create a room_id
    #     """

    #     if rd and not room_id:
    #         room_id = rd.room_id
    #     else:
    #         rd = self._client.rooms[room_id]

    #     room_data = RoomDataFactory().room_data(room_id)
    #     room_data.room_id = room_id
    #     room_data.name = rd.name
    #     pyotherside.send("syncingRooms", rd.display_name)
    #     room_data.display_name = rd.display_name
    #     room_data.topic = rd.topic
    #     tags = rd.get_tags()["tags"]
    #     for tag in tags:
    #         if tag == "m.favourite":
    #             room_data.level = "fav"
    #             room_data.lorder = tags[tag].get("order", 0)
    #         elif tag == "m.lowpriority":
    #             room_data.level = "low"
    #         else:
    #             room_data.level = "normal"

    #     events = rd.get_events()
    #     room_data.events = []
    #     for event in events:
    #         self.room_append_event(room_id, event, save=False)

    #     if room_id in self._data.roomdir:
    #         for member in rd.get_joined_members():
    #             if member.user_id != self._data.user_id:
    #                 try:
    #                     avatar_url = member.get_avatar_url()
    #                     user_id = member.user_id
    #                     if avatar_url:
    #                         room_data.avatar_url = self._get_url(avatar_url)
    #                 except:
    #                     avatar_url = ""
    #                     user_id = ""
    #     else:
    #         for e in self._client.api.get_room_state(room_id):
    #             if e["type"] == "m.room.avatar":
    #                 aurl = e["content"]["url"]
    #                 if aurl:
    #                     avatar_url = self._client.api.get_download_url(aurl)
    #                     room_data.avatar_url = self._get_url(avatar_url)
    #         user_id = room_id

    #     room_data.user_id = user_id
    #     room_data.encrypted = rd.encrypted
    #     room_data.end_token = self._client.api.get_room_messages(
    #         room_id, None, "f"
    #     )["end"]
    #     room_data.start_token = self._client.api.get_room_messages(
    #         room_id, None, "f"
    #     )["start"]
    #     room_data.members = self.get_room_members(room_id)

    #     room_data.save()

    # def _sync_room(self, room_id: str):
    #     """
    #     Sync room data
    #     """

    #     rd = self._client.rooms[room_id]
    #     room_data = RoomDataFactory().room_data(room_id)
    #     if not room_data.room_id:
    #         self._create_room(room_id)
    #         return

    #     room_data.name = rd.name
    #     room_data.display_name = rd.display_name
    #     room_data.topic = rd.topic
    #     tags = rd.get_tags()["tags"]
    #     for tag in tags:
    #         if tag == "m.favourite":
    #             room_data.level = "fav"
    #             room_data.lorder = tags[tag]["order"]
    #         elif tag == "m.lowpriority":
    #             room_data.level = "low"
    #         else:
    #             room_data.level = "normal"
    #     events = self._client.api.get_room_messages(
    #         room_id, room_data.end_token, "f"
    #     )["chunk"]
    #     for event in events:
    #         pyotherside.send("newEvent")
    #         self.room_append_event(room_id, event, save=False)

    #     room_data.end_token = self._client.api.get_room_messages(
    #         room_id, None, "f"
    #     )["end"]
    #     room_data.save()

    # def sync_rooms(self):
    #     """
    #     Synchronize rooms from last time
    #     """

    #     self._data.rooms = []
    #     for room in self._client.rooms:
    #         self._sync_room(room)
    #         self._data.rooms.append(room)

    #     pyotherside.send("initReady", self._data.user_id)

    # def get_rooms(self):
    #     """
    #     Initial room fetching and preparing room_data objects
    #     """

    #     for room in self._client.rooms:
    #         self._create_room(room)
    #         self._data.rooms.append(room)

    # def get_roomlist(self):
    #     """
    #     Get roomlisst
    #     """

    #     for room in self._data.rooms:
    #         rd = Memory().get_object(room)
    #         pyotherside.send("log", "outputting", rd.room_id)
    #         if rd:
    #             levent = rd.events[len(rd.events) - 1] if rd.events else None
    #             yield {
    #                 "room_id": rd.room_id,
    #                 "user_id": rd.user_id,
    #                 "room_name": rd.display_name,
    #                 "prio": rd.level if rd.level else "",
    #                 "direct": room in self._data.roomdir,
    #                 "avatar_url": rd.avatar_url,
    #                 "time": rd.last_time,
    #                 "encrypted": rd.encrypted,
    #                 "last_event": levent,
    #             }

    # def get_room(self, room_id: str):
    #     """
    #     get single room data
    #     """
    #     rd = Memory().get_object(room_id)
    #     if rd:
    #         return {
    #             "room_id": rd.room_id,
    #             "user_id": rd.user_id,
    #             "room_name": rd.display_name,
    #             "prio": rd.level,
    #             "direct": room_id in self._data.roomdir,
    #             "avatar_url": rd.avatar_url,
    #             "time": rd.last_time,
    #             "encrypted": rd.encrypted,
    #         }

    #     return None

    # def listener_exception(self, e):
    #     """
    #     Listener exception
    #     """

    #     pyotherside.send("Listener exception")
    #     # pyotherside.send("restartListener")

    # def seen_message(self, room_id: str):
    #     """
    #     Send message receipt
    #     """

    #     rd = RoomDataFactory().room_data(room_id)

    #     if not rd:
    #         return

    #     levent = rd.events[len(rd.events) - 1]["event"]

    #     try:
    #         lr = rd.last_read
    #     except:
    #         lr = 0
    #     if lr < levent["origin_server_ts"]:
    #         pyotherside.send("seen_message")
    #         rd.last_read = levent["origin_server_ts"]
    #         self._client.api.send_read_markers(
    #             room_id, levent["event_id"], levent["event_id"]
    #         )
    #         rd.save()

    # def is_typing(self, room_id: str, timeout: int = 30000):
    #     """
    #     Send typing
    #     Args:
    #         room_id(str): The room ID.
    #         timeout: Timeout of typing
    #     """

    #     content = {"typing": True, "timeout": timeout}
    #     path = "/rooms/{}/typing/{}".format(
    #         quote(room_id), quote(self._data.user_id)
    #     )
    #     self._client.api._send("PUT", path, content)

    # def start_listener(self):
    #     """
    #     start listener_thread
    #     """

    #     try:
    #         self._client.start_listener_thread(
    #             timeout_ms=60000, exception_handler=self.listener_exception
    #         )
    #     except:
    #         pyotherside.send("Starting listener thread failed")
    #         pyotherside.send("restartListener")

    # def restart_listener(self):
    #     """
    #     Restart the listener thread
    #     """

    #     try:
    #         self.stop_listener()
    #     except:
    #         pyotherside.send("log", "restartListener: cannot stop listener")
    #     try:
    #         self.start_listener()
    #     except:
    #         pyotherside.send("log", "restartListener: cannot start listener")

    # def stop_listener(self):
    #     """
    #     stop the listener_thread
    #     """

    #     try:
    #         self.client._stop_listener_thread()
    #     except:
    #         pyotherside.send("Stopping listener thread failed")

    # def room_listener(self, event):
    #     """
    #     The room listener
    #     """

    #     room_id = event["room_id"]
    #     rd = Memory().get_object(room_id)
    #     if event["type"] == "m.room.redaction":
    #         count = 0
    #         for ev in rd.events:
    #             if ev["event"]["event_id"] == event["redacts"]:
    #                 # event["event_id"] = event["redacts"]
    #                 dat, rd.events[count] = self.format_event(room_id, event)
    #                 rd.last_event = dat
    #                 rd.save()
    #                 pyotherside.send("mRedactEvent", room_id, event)
    #                 return
    #             count += 1

    #     event_data = self.room_append_event(room_id, event)
    #     if event_data["user_id"] == self._data.user_id:
    #         rd.set_revent(event_data["event"]["event_id"])

    #     pyotherside.send("mRoomEvent", room_id, event_data)
    #     pyotherside.send("roomUnread", event["room_id"], rd.unread_count)

    # def receipt_listener(self, event):
    #     """
    #     A user has read some event
    #     """

    #     for receipt in event["content"]:
    #         for uid in event["content"][receipt]["m.read"]:
    #             if uid == self._data.user_id:
    #                 rd = Memory().get_object(event["room_id"])
    #                 rd.set_revent(receipt)
    #                 pyotherside.send(
    #                     "roomUnread", event["room_id"], rd.unread_count
    #                 )

    # def get_unread_count(self, room_id: str):
    #     rd = Memory().get_object(room_id)

    #     if rd:
    #         pyotherside.send("roomUnread", room_id, rd.unread_count)

    # def typing_listener(self, event):
    #     """
    #     A user is typing (or not)
    #     """

    #     if len(event["content"]["user_ids"]) == 0:
    #         pyotherside.send("stopTyping", event["room_id"])
    #         return

    #     users = []

    #     for user_id in event["content"]["user_ids"]:
    #         user = self._client.get_user(user_id)
    #         user_name = user.get_display_name()
    #         avatar_url = user.get_avatar_url()
    #         if not avatar_url:
    #             image = ""
    #         else:
    #             image = self._get_url(avatar_url)

    #         users.append(
    #             {"user_id": user_id, "user_name": user_name, "image": image}
    #         )
    #     pyotherside.send("startTyping", event["room_id"], users)

    # def stop_room_event_listener(self):
    #     """
    #     Leave listener
    #     """

    #     self._client.remove_listener(self._m_room_message_listener)
    #     self._client.remove_listener(self._m_room_sticker_listener)
    #     self._client.remove_listener(self._m_redaction_listener)
    #     self._client.remove_ephemeral_listener(self._m_typing_listener)
    #     self._client.remove_ephemeral_listener(self._m.receipt_listener)
    #     self._m_room_message_listener = None
    #     self._m_room_sticker_listener = None
    #     self._m.redaction_listener = None
    #     self._m_typing_listener = None
    #     self._m_receipt_listener = None

    # def start_room_event_listener(self):
    #     """
    #     set room listener
    #     """

    #     if self._m_room_message_listener:
    #         self._client.remove_listener(self._m_room_message_listener)
    #     if self._m_room_sticker_listener:
    #         self._client.remove_listener(self._m_room_sticker_listener)
    #     if self._m_redaction_listener:
    #         self._client.remove_listener(self._m_redaction_listener)
    #     self._m_room_message_listener = self._client.add_listener(
    #         self.room_listener, event_type="m.room.message"
    #     )
    #     self._m_room_sticker_listener = self._client.add_listener(
    #         self.room_listener, event_type="m.sticker"
    #     )
    #     self._m_redaction_listener = self._client.add_listener(
    #         self.room_listener, event_type="m.room.redaction"
    #     )
    #     self._m_typing_listener = self._client.add_ephemeral_listener(
    #         self.typing_listener, event_type="m.typing"
    #     )
    #     self._m_receipt_listener = self._client.add_ephemeral_listener(
    #         self.receipt_listener, event_type="m.receipt"
    #     )

    # def get_room_members(self, room_id: Optional[str]):
    #     """
    #     get members and event list for room_id
    #     """

    #     members = []
    #     for member in self._client.api.get_room_members(room_id)["chunk"]:
    #         try:
    #             if member["content"]["avatar_url"]:
    #                 image = self._get_url(member["content"]["avatar_url"])
    #             else:
    #                 image = ""
    #         except:
    #             image = ""

    #         try:
    #             member["origin_server_ts"] /= 1000
    #             members.append(
    #                 {
    #                     "event_id": member["event_id"],
    #                     "name": member["content"]["displayname"],
    #                     "avatar_url": image,
    #                     "time": member["origin_server_ts"],
    #                 }
    #             )
    #         except:
    #             pass

    #     return members

    # def get_room_events(self, room_id: str):
    #     """
    #     get room events for room_id
    #     """

    #     rd = Memory().get_object(room_id)
    #     events = rd.get_room_events()
    #     members = rd.members
    #     pyotherside.send("roomEvents", events, members)

    # def room_member_list(self, room_id: str):
    #     """
    #     Send room member list
    #     """
    #     rd = Memory().get_object(room_id)
    #     pyotherside.send("roomMembers", rd.members)

    # def get_user_info(self):
    #     pyotherside.send(
    #         "userInfo",
    #         {
    #             "lname": self._data.user_name,
    #             "user_id": self._data.user_id,
    #             "avatar_url": self._data.user_avatar_url,
    #         },
    #     )

    # def _get_url(self, url: str):
    #     """
    #     """

    #     if url == "":
    #         return url

    #     f = urlparse(url).path
    #     filename = f[f.rfind("/") + 1 :]
    #     path = os.path.join(Memory().cache_home, filename)
    #     if not os.path.exists(path):
    #         pyotherside.send("get_url: need to download", path)
    #         try:
    #             util.dl_from_url(url, path)
    #         except:
    #             return ""

    #     return path

    # # def sync(self, first: bool = False):
    # #     """
    # #     Sync with server
    # #     """

    # #     pyotherside.send("Client data syncer")
    # #     try:
    # #         syncdata = self._client.api.sync(
    # #             since=self._ts_token, full_state=first
    # #         )
    # #     except:
    # #         syncdata = None

    # #     if not syncdata:
    # #         pyotherside.send("Matrix syncing failed")
    # #         return

    # #     event = next(
    # #         filter(
    # #             lambda x: x["type"] == "com.gitlab.cy8aer.bluepill",
    # #             None
    # #             # account_data["events"],
    # #         ),
    # #         None,
    # #     )
    # #     if event:
    # #         pyotherside.send(
    # #             "log", "com.gitlab.cy8aer.bluepill in account_data"
    # #         )
    # #         pass

    # #     for room in syncdata["rooms"]["join"]:
    # #         # configuration of rooms
    # #         pass

    # #     for room in syncdata["rooms"]["invite"]:
    # #         # you are invited to new rooms
    # #         pass

    # #     for room in syncdata["rooms"]["leave"]:
    # #         # you left rooms or you have been banned from a room
    # #         pass

    # #     for presence in syncdata["presence"]["events"]:
    # #         # presence of users
    # #         # content:
    # #         #     currently_active
    # #         #     last_active_ago
    # #         #     presence
    # #         # sender
    # #         # type
    # #         pass

    # #     # device_lists

    # #     self.get_room_dir(syncdata)

    # #     # device_time_keys_count

    # def save(self):
    #     """
    #     Save myself
    #     """

    #     # Memory().save_object(STORE_NAME, self)

    # # Data providers

    # def send_message(self, room_id: str, message: str):
    #     """
    #     Markdown transform a message and send it
    #     """

    #     message = emoji.emojize(message, use_aliases=True)
    #     pyotherside.send("send_message", room_id, message)
    #     htmlmsg = self._markdown.convert(message)
    #     room = self._client.rooms[room_id]
    #     pyotherside.send("sending message ", htmlmsg, message)
    #     room.send_html(htmlmsg, body=message)

    # def send_image(self, room_id: str, image_file: str):
    #     """
    #     upload an image and send a message
    #     """

    #     mimes = {
    #         "jpg": "image/jpeg",
    #         "jpeg": "image/jpeg",
    #         "gif": "image/gif",
    #         "png": "image/png",
    #     }

    #     ext = os.path.splitext(image_file)[1][1:]
    #     content_type = mimes[ext]

    #     pyotherside.send("log", "uploading image")
    #     try:
    #         with open(image_file, "rb") as image:
    #             mxc = self._client.upload(image.read(), content_type)
    #     except MatrixUnexpectedResponse:
    #         pyotherside.send(
    #             "imageSendFailed", "image upload unexpected response"
    #         )
    #         return
    #     except MatrixRequestError:
    #         pyotherside.send("imageSendFailed", "image upload request error")
    #         return
    #     except:
    #         pyotherside.send("imageSendFailed", "other error")
    #         return

    #     pyotherside.send("log", "send image message")
    #     room = self._client.rooms[room_id]
    #     try:
    #         room.send_image(mxc, os.path.split(image_file)[1])
    #     except:
    #         pyotherside.send("imageSendFailed", "image message failed")

    #     pyotherside.send("imageSent")

    # def redact(self, room_id: str, event_id: str, reason: str = None):
    #     """
    #     Redact a message
    #     """

    #     rd = self._client.rooms[room_id]
    #     rd.redact_message(event_id, reason)

    # @property
    # def user_name(self):
    #     """
    #     Return the user_name
    #     """

    #     return self._user_name

    # @property
    # def user_id(self):
    #     """
    #     Return the user_id
    #     """

    #     return self._user_id


class BluepillClientFactory(metaclass=Singleton):
    """
    Factory which creates a BluepillClient if it does not exist
    """

    def __init__(self):
        """
        Initialization
        """

        self._bluepill_client = None

    def get_client(self):
        """
        get the Bluepill Client
        """

        if not self._bluepill_client:
            self._bluepill_client = BluepillClient()

        return self._bluepill_client
