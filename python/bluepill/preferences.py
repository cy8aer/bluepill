"""
Preferences Key/Value store
"""

import sys
from peewee import CharField

sys.path.append("../")

from bluepill.constants import Constants, BaseModel, db

class Preferences(BaseModel):
    """
    Preferences Key/Value model
    """

    key: str = CharField(primary_key=True)
    value: str = CharField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

