"""
Bluepill timeline Bucket

This is a chunk of timeline data.
"""

import sys
from typing import List

sys.path.append("../")
sys.path.append("/usr/share/harbour-bluepill/lib/python3.7/site-package")

import attr


class BucketFull(BaseException):
    pass


@attr.s
class TimelineBucket(object):
    """
    Timeline Bucket
    """

    room_id = attr.ib()
    events: List = attr.ib(default=[])
    reverse = attr.ib(default=True)
    next_batch = attr.ib(default=None)
    last_batch = attr.ib(default=None)
    max_events = attr.ib(default=10)

    def get_events(self, reverse: bool = False):
        """
        Get the event list

        Args:

            reverse (bool): True if events should be generated
                in reversed order
        Returns:
            event generator of events
        """

        dorev = reverse != self.reverse

        if dorev:
            return (event for event in reversed(self.events))
        else:
            return (event for event in self.events)

    def add_event(self, event, last_batch: str = None):
        """
        add an event to the event list

        Args:
            event (RoomMessageFormatted): the event to be appended
            last_batch (str): new last_batch pointer

        Returns:
            bucket state (True cold, False: hot)
        Raises:
            BucketFull if bucket is already filled
        """

        if len(self.events) >= self.max_events:
            raise BucketFull

        self.events.append(event)
        if last_batch:
            self.last_batch = last_batch

        return len(self.events) >= self.max_events

    def change_event(self, event_id: str, newevent):
        """
        change an event with another

        Args:
            event_id (str): Id of the event to be changed
            newevent (RoomMessageFormatted): the new event
        """

        pass

    @property
    def get_batches(self):
        """
        Return the start and end batch of bucket
        """

        return self.last_batch, self.next_batch

    @property
    def get_times(self):
        """
        Get the start time and end time of the bucket
        """

        if len(self.events) > 0:
            if self.reversed is True:
                start = self.events[-1].server_timestamp
                stop = self.events[0].server_timestamp
            else:
                start = self.events[0].server_timestamp
                stop = self.events[-1].server_timestamp
        else:
            start = None
            stop = None

        return start, stop

    @property
    def get_size(self):
        """
        Get Bucket size
        """

        return len(self.events)
