"""
A memory cache with limited elements
"""
from collections import OrderedDict
import hashlib
from typing import Any, Union


class Cache:
    def __init__(self, limit: int = 10):
        """
        Initialize the class.
        Args:
            limit (str): limit of elements to be stored
        """

        self.limit = limit
        self.elements = OrderedDict()  # type: OrderedDict

    def _hashme(self, name: str) -> str:
        """
        Create a hash from name
        Args:
            name (str): the name to be hashed
        Returns:
            hash file name (ending with ".pickle")
        """

        return hashlib.sha256(name.encode()).hexdigest() + ".pickle"

    def _clean(self) -> None:
        """
        Cleanup cache if length > limit to limit size.
        """

        while len(self.elements) > self.limit:
            self.elements.popitem(last=False)

    def store(self, index: str, element: Any) -> None:
        """
        Store the element in self.elements
        Args:
            index (str): Element index
            element (Any): the element. Can be any type
        """

        fname = self._hashme(index)
        self.elements[fname] = element
        self._clean()

    def delete(self, index: str) -> None:
        """
        Delete an element from Cache
        Args:
            index (str): Element index
        """

        fname = self._hashme(index)
        if fname in self.elements:
            del self.elements[fname]

    def get(self, index: str) -> Union[Any, None]:
        """
        Get an element from cache
        Args:
            index (str): Element index
        Returns:
            The element (Any type) or None
        """

        fname = self._hashme(index)
        if fname in self.elements:
            return self.elements[fname]
        return None

    def exists(self, index: str) -> bool:
        """
        Does element exist?
        Args:
            index (str): Element index
        Returns:
            True if exists
        """

        fname = self._hashme(index)
        return fname in self.elements

    @property
    def size(self) -> int:
        """
        Return cache size
        Returns:
            The cache size
        """

        return len(self.elements)
