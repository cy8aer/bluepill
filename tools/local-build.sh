#!/bin/bash

if [ -n "$1" ]
then
    ARCH=$1
  else
      ARCH='armv7hl'
fi

# OLMVERSION=3.2.1
SFVERSION=4.0.1.45

echo "copying source"
mkdir -p ~nemo/bluepill
rsync -a /source/* ~nemo/bluepill
echo "done"

cd ~nemo/bluepill || exit

mb2 -t SailfishOS-$SFVERSION-$ARCH build
cp RPMS/*rpm /source/tmp

