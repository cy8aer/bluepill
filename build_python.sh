#!/bin/bash
# This is done via coderus' docker container
# in bluepill root directory just call:
# ./build_python.sh

pushd build
sudo rm -rf bin include lib pyvenv.cfg
popd

docker run -v $(pwd)/build:/home/nemo/build --rm --name=sdk -ti \
    coderus/sailfishos-platform-sdk:3.3.0.14 /home/nemo/build/build.sh

sudo chown -R "$(id -u)":"$(id -g)" build
