<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>CoverPage</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>[sticker]</source>
        <translation>[sticker]</translation>
    </message>
    <message>
        <source>[encrypted message]</source>
        <translation>[verschlüsselte Nachricht]</translation>
    </message>
    <message>
        <source>[message redacted]</source>
        <translation>[Nachricht zensiert]</translation>
    </message>
    <message>
        <source>[unknown]</source>
        <translation>[unbekannt]</translation>
    </message>
    <message>
        <source>is typing...</source>
        <translation>tippt...</translation>
    </message>
</context>
<context>
    <name>DashboardPage</name>
    <message>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Rooms</source>
        <translation>Räume</translation>
    </message>
    <message>
        <source>Create room</source>
        <translation>Raum erstellen</translation>
    </message>
    <message>
        <source>Enter room</source>
        <translation>Raum betreten</translation>
    </message>
    <message>
        <source>StartChat</source>
        <translation>Gespräch beginnen</translation>
    </message>
    <message>
        <source>Direct Messages</source>
        <translation>Direkte Nachrichten</translation>
    </message>
</context>
<context>
    <name>DeleteDevice</name>
    <message>
        <source>Delete device</source>
        <translation>Gerät löschen</translation>
    </message>
    <message>
        <source>Device ID</source>
        <translation>Geräte-ID</translation>
    </message>
    <message>
        <source>Device Name</source>
        <translation>Gerätename</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
</context>
<context>
    <name>DeviceListItem</name>
    <message>
        <source>Verify with emojis</source>
        <translation>Mit Emojis verifizieren</translation>
    </message>
    <message>
        <source>Text verify</source>
        <translation>Mit Text verifizieren</translation>
    </message>
    <message>
        <source>Delete device</source>
        <translation>Gerät löschen</translation>
    </message>
</context>
<context>
    <name>EventsListItem</name>
    <message>
        <source>copy</source>
        <translation>kopieren</translation>
    </message>
    <message>
        <source>event source</source>
        <translation>Ereignis-Quelle</translation>
    </message>
    <message>
        <source>cite</source>
        <translation>Zitat</translation>
    </message>
    <message>
        <source>delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <source>End to end encryption not implemented</source>
        <translation>Ende-zu-Ende-Verschlüsselung ist nicht implementiert</translation>
    </message>
    <message>
        <source>redacted</source>
        <translation>herausgegeben</translation>
    </message>
    <message>
        <source>left the room</source>
        <translation>hat den Raum verlassen</translation>
    </message>
    <message>
        <source>is now </source>
        <translation>ist nun</translation>
    </message>
    <message>
        <source>entered the room</source>
        <translation>hat den Raum betreten</translation>
    </message>
</context>
<context>
    <name>JoinRoom</name>
    <message>
        <source>Join Room</source>
        <translation>Raum betreten</translation>
    </message>
</context>
<context>
    <name>KeyVerificationKey</name>
    <message>
        <source>Compare Icons</source>
        <translation>Vergleiche Bilder</translation>
    </message>
</context>
<context>
    <name>KeyVerificationStart</name>
    <message>
        <source>Accept Verification?</source>
        <translation>Akzeptiere Überprüfung?</translation>
    </message>
    <message>
        <source>wants to verify this device.</source>
        <translation>möchte dieses Gerät überprüfen</translation>
    </message>
</context>
<context>
    <name>LogStatusPage</name>
    <message>
        <source>First sync. This may take some time...</source>
        <translation>Erste Synchronisation. Das kann etwas dauern...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Client Name</source>
        <translation>Gerätename</translation>
    </message>
    <message>
        <source>Custom Server</source>
        <translation>Individueller Server</translation>
    </message>
    <message>
        <source>Home server URL</source>
        <translation>Server URL</translation>
    </message>
    <message>
        <source>https://matrix.org</source>
        <translation>https://matrix.org</translation>
    </message>
    <message>
        <source>Register</source>
        <translation>Registrieren</translation>
    </message>
    <message>
        <source>Login error</source>
        <translation>Login-Fehler</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Abmelden</translation>
    </message>
    <message>
        <source>Logging out</source>
        <translation>Melde mich ab</translation>
    </message>
    <message>
        <source>Font size</source>
        <translation>Zeichensatzgröße</translation>
    </message>
    <message>
        <source>Markdown rendering</source>
        <translation>Markdown-Formatierung</translation>
    </message>
    <message>
        <source>Render messages with Markdown</source>
        <translation>Formatiere Nachrichten mit Markdown</translation>
    </message>
    <message>
        <source>Only notify for new messages from favourite rooms</source>
        <translation>Benachrichtige nur bei Nachrichten von Favoriten-Räumen</translation>
    </message>
    <message>
        <source>Notify favourites only</source>
        <translation>Benachrichtige nur Favoriten</translation>
    </message>
    <message>
        <source>User settings</source>
        <translation>Benutzereinstellungen</translation>
    </message>
    <message>
        <source>Security</source>
        <translation>Sicherheit</translation>
    </message>
    <message>
        <source>My Sessions</source>
        <translation>Meine Sitzungen</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Nachrichten</translation>
    </message>
</context>
<context>
    <name>RedactMessage</name>
    <message>
        <source>Reason for redaction</source>
        <translation>Grund der Zensur</translation>
    </message>
    <message>
        <source>Reason</source>
        <translation>Grund</translation>
    </message>
    <message>
        <source>Do you really wish to redact (delete) this event? This cannot be undone.</source>
        <translation>Möchtest Du die Nachricht zensieren (löschen)? Das kann nicht rückgängig gemacht werden.</translation>
    </message>
</context>
<context>
    <name>RegisterPage</name>
    <message>
        <source>Register</source>
        <translation>Registrieren</translation>
    </message>
    <message>
        <source>Email address (optional)</source>
        <translation>E-Mail-Adresse (Optional)</translation>
    </message>
</context>
<context>
    <name>RoomListItem</name>
    <message>
        <source>Leave</source>
        <translation>Verlassen</translation>
    </message>
    <message>
        <source>Favourite</source>
        <translation>Favorit</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Low priority</source>
        <translation>Niedrige Priorität</translation>
    </message>
    <message>
        <source>leaving room</source>
        <translation>verlasse Raum</translation>
    </message>
</context>
<context>
    <name>RoomPage</name>
    <message>
        <source>Preferences: </source>
        <translation>Einstellungen: </translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <source>Starting engine...</source>
        <translation>Starte Funktionseinheit...</translation>
    </message>
    <message>
        <source>Connected...</source>
        <translation>Verbunden...</translation>
    </message>
    <message>
        <source>Not connected, trying again later...</source>
        <translation>Nicht verbunden, versuche es später nochmal...</translation>
    </message>
</context>
<context>
    <name>TextControl</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source> is typing...</source>
        <translation> tippt...</translation>
    </message>
</context>
<context>
    <name>TextVerification</name>
    <message>
        <source>Verify device</source>
        <translation>Gerät verifizieren</translation>
    </message>
    <message>
        <source>Device ID</source>
        <translation>Geräte-ID</translation>
    </message>
    <message>
        <source>Device Name</source>
        <translation>Gerätename</translation>
    </message>
    <message>
        <source>User ID</source>
        <translation>Anwender-ID</translation>
    </message>
    <message>
        <source>Session Key</source>
        <translation>Sitzungsschlüssel</translation>
    </message>
</context>
<context>
    <name>UploadSelector</name>
    <message>
        <source>Upload...</source>
        <translation>Hochladen...</translation>
    </message>
    <message>
        <source>Take a picture</source>
        <translation>Foto machen</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Bild</translation>
    </message>
    <message>
        <source>Send image</source>
        <translation>Sende Bild</translation>
    </message>
</context>
<context>
    <name>UserSessions</name>
    <message>
        <source>Devices of </source>
        <translation>Geräte von </translation>
    </message>
    <message>
        <source>Login error</source>
        <translation>Login-Fehler</translation>
    </message>
    <message>
        <source>Wrong password</source>
        <translation>Falsches Passwort</translation>
    </message>
</context>
<context>
    <name>harbour-bluepill</name>
    <message>
        <source>Click to view updates</source>
        <translation>Klicke um die Neugkeiten zu lesen</translation>
    </message>
    <message>
        <source>New Posts are available. Click to view.</source>
        <translation>Neue Nachrichten verfügbar. Klicke um sie zu sehen.</translation>
    </message>
    <message>
        <source>New posts available</source>
        <translation>Neue Nachrichten verfügbar</translation>
    </message>
    <message>
        <source>Room login failed</source>
        <translation>Betreten des Raums gescheitert</translation>
    </message>
    <message>
        <source>1 minute ago</source>
        <translation>vor einer Minute</translation>
    </message>
    <message>
        <source></source>
        <translation>vor </translation>
    </message>
    <message>
        <source> minutes ago</source>
        <translation> Minuten</translation>
    </message>
    <message>
        <source> hours ago</source>
        <translation> Stunden</translation>
    </message>
    <message>
        <source>Yesterday</source>
        <translation>Gestern</translation>
    </message>
    <message>
        <source> days ago</source>
        <translation> Tagen</translation>
    </message>
    <message>
        <source> weeks ago</source>
        <translation> Wochen</translation>
    </message>
    <message>
        <source>just now</source>
        <translation>gerade</translation>
    </message>
    <message>
        <source>1 hour ago</source>
        <translation>vor einer Stunde</translation>
    </message>
    <message>
        <source>never</source>
        <translation>nie</translation>
    </message>
</context>
</TS>
